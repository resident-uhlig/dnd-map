# dnd-map

<a href="https://gitlab.com/resident-uhlig/legacy-code"><img alt="Legacy Code: O++ S++ I C-- E-- M V--- !PS D+ Ab" src="https://img.shields.io/badge/Legacy%20Code-O%2B%2B%20S%2B%2B%20I%20C----%20E----%20M%20V------%20!PS%20D%2B%20Ab-informational"></a>

## About

This is an **unofficial** (see [License](#license)) interactive map for the planet [Toril][Toril] from the fantasy universe of the [Forgotten Realms][Forgotten Realms] in Dungeon & Dragons.

[Toril]: https://forgottenrealms.fandom.com/wiki/Toril
[Forgotten Realms]: https://forgottenrealms.fandom.com/wiki/Forgotten_Realms

## Requirements

- Modern browser

  - Modern [ECMAScript][ECMAScript]
  - [canvas][canvas] with 2D rendering context

- Image files for maps in `src/maps/` as referenced in [maps.json](src/data/maps.json).

[ECMAScript]: https://262.ecma-international.org/
[canvas]: https://html.spec.whatwg.org/multipage/canvas.html

## Usage

Open [index.html](src/index.html) in your browser.

## Dependencies

There are basically no dependencies at runtime because everything is written in vanilla JavaScript. However, there are some dependencies for developing purpose, that are managed using [npm][npm] in [package.json](package.json).

[npm]: https://www.npmjs.com/

## Development

- [Backlog](Backlog.md)

### Prepare the local source code

1. `git clone` the repository.
2. Run `npm install` in the repository root.

### Update dependencies

1. `ncu` checks the dependencies.
2. `ncu -u` actually updates the dependencies.

### Code style

Use [.editorconfig](.editorconfig) for pre-formatting the source code in your IDE.
Enable git hooks to automatically format the code according to [.prettierrc](.prettierrc) while commiting changes.

### Run tests

Open [test.html](src/test.html) in your browser.

## License

This project is unofficial and not affiliated with Wizards of the Coast LLC in
any way
([Wizards of the Coast Fan Content Policy](https://company.wizards.com/fancontentpolicy),
[Dungeons & Dragons System Reference Document](https://dnd.wizards.com/resources/systems-reference-document)).

This prioject uses Font Awesome Free icons
([Font Awesome Free License](https://fontawesome.com/license/free)).

Regarding the source code, the following applies:

> Copyright (c) 2024 Sven Uhlig git@resident-uhlig.de
>
> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:
>
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
>
> The Software shall be used for Good and must NOT be used for Evil.
>
> The Software must NOT be used to operate nuclear facilities, weapons, things
> owned by a state or its contractors, life support or mission-critical
> applications where human life or property may be at stake.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

[Impressum](https://resident-uhlig.de/impressum.html)
