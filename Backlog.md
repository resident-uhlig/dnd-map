# Backlog

## Prioritized

- feat: tokens for battle map grid size
- feat: tool to move and scale maps (including their landmarks)
- refactor: remove redundancy in code of de_DE_format.mjs and rules/convert.mjs

## To be prioritized

- feat(test): deep link into source files ( jetbrains://idea/navigate/reference?project=dnd-map&path=src/js/html/html.test.mjs - https://stackoverflow.com/questions/56061831/create-a-web-browser-link-that-opens-a-file-in-intellij-possible)
- feat(test): toggle for light/dark mode
- feat: add all POIs in data
- feat: add option to select mount and display travel time for measure tool
- feat: align to grid for measure tool
- feat: ambient audio sources
- feat: confirm copy to clipboard for export in POI editor
- feat: confirm export of missing I18N keys
- feat: confirm share link
- feat: confirm that position was copied in get position tool
- feat: different shapes for measure tool
- feat: error handling for fullscreen activate/deactivate
- feat: go to poi with hitting enter when there is only one result
- feat: history of X/Y and zoom level
- feat: i18n for title element and lang attribute
- feat: language selector
- feat: long text/html for POIs
- feat: mobile support for measurement tool
- feat: move existing nodes for measure tool
- feat: remove node in between for measure tool
- feat: rotate map images for initial positioning (was there before but removed due to refactor/rewrite)
- feat: rotate map view
- feat: search for POI in a specific map
- feat: select closest map for POI editor when editor activated
- feat: show regexp errors to user for search of POIs
- feat: show sources for image files
- feat: soft edges for maps (was there before but removed due to refactor/rewrite)
- feat: sort map layers by localized name
- feat: sync view over network
- feat: sync view with another browser/user
- feat: timeline for map images
- feat: toggle display of names for POIs
- feat: use icons instead of text for key bindings (and use alt="text" for a11y)
- fix: icons for map landmarks too small on vertical mobile device, can't be clicked
- fix: native zoom breaks view and hides toolbar, helpbar etc. (as seen on Philipp's Windows laptop in Chrome)
- fix: scaling seems to be of, see map of neverwinter
- fix: sync view in chain does not actually sync from first window to last window in chain
- fix: tool settings is overlayed by help bar
- performance(useState,html): need to unsubscribe when elements are not part of DOM anymore
- performance: improve handling of observable queue in preloading ui to only update progress elements instead of removing and adding them again and again
- performance: maybe calculate sx sy sw sh when drawing maps if it makes any difference
- performance: moving map with POIs is slow/laggy
- performance: only add free form node if position differs from last node
- performance: only draw map images that are visible
- performance: only render landmarks that are within visible rect for interact tool
- performance: only render landmarks that should be shown (???)
- performance: remove free form if it has less than two nodes
- refactor(html): compact both loops to one single XPath evaluation in order to avoid replacing "placeholder" values that actually are plain values. (use solution from dnd-story)
- refactor(tests): don't use alert() for reporting top level errors
- refactor: in preload there is state as observable and queue as native array.
- refactor: interact filters maps and landmarks multiple times, apply DRY
- refactor: maybe don't addeventlistener to window / document inside of render functions if possible
- refactor: maybe merge share tools
- refactor: use more early returns
- style: naming conventions for variables (don't use snake_case, use camelCase instead)

## Won't implement

- feat(css): maybe use shadow/background/border for tool bar only around li-s so less of the map is covered?

  Won't implement because without shadow the tool bar would still occupy the space but would not be visible to the user so events would not work without visible cause for the user.

- feat: limit panning to map boundaries
- feat: limit zoom to map resolution
