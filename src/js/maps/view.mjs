import useState from "../state/useState.mjs";

const [view, setView] = useState({});

export default view;
export {setView};

export function goto({ x_mi, y_mi, pxPerMi }) {
  setView((view) => ({ ...view, x_mi, y_mi, pxPerMi }));
}
