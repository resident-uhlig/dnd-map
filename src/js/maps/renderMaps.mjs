import drawMaps from "./drawMaps.mjs";
import createFullsizeCanvas from "../canvas/createFullsizeCanvas.mjs";
import useStates from "../state/useStates.mjs";
import {activeMaps} from "./maps.mjs";
import view from "./view.mjs";

export default function renderMaps() {
  const [canvas, requestDraw] = createFullsizeCanvas((context) =>
    drawMaps(context, activeMaps(), view()),
  );
  canvas.classList.add("map");

  useStates(view, activeMaps)(requestDraw);
  return canvas;
}
