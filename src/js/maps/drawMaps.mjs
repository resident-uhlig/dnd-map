import drawAndRestore from "../canvas/drawAndRestore.mjs";

function drawMap(
  context,
  { name, image, center, scale },
  { x_mi, y_mi, pxPerMi },
) {
  const { width, height } = context.canvas;

  // original px to original miles to scaled px
  const dw_px = image.width * scale.miPerPx * pxPerMi;
  if (dw_px < 1) {
    return;
  }

  const dh_px = image.height * scale.miPerPx * pxPerMi;
  if (dh_px < 1) {
    return;
  }

  // offset from position in miles centered to offset of canvas
  const dx_px = (center.x_mi - x_mi) * pxPerMi + (width - dw_px) / 2;
  if (dx_px + dw_px < 0 || dx_px > width) {
    return;
  }

  const dy_px = (center.y_mi - y_mi) * pxPerMi + (height - dh_px) / 2;
  if (dy_px + dh_px < 0 || dy_px > height) {
    return;
  }

  drawAndRestore(context, (context) => {
    const scaleRatio = pxPerMi / scale.pxPerMi;
    const widthRatio = dw_px / width;
    const heightRatio = dh_px / height;
    context.globalAlpha = getAlpha(scaleRatio, widthRatio, heightRatio);
    context.drawImage(image, dx_px, dy_px, dw_px, dh_px);
  });
}

function getAlpha(scaleRatio, widthRatio, heightRatio) {
  // if the resolution of a map image is lower than the current zoom level, then the image should be completely opaque
  // if width or height of a map image is cropped, then the image should be completely opaque
  if (scaleRatio >= 1 || widthRatio >= 1 || heightRatio >= 1) {
    return 1;
  }

  if (scaleRatio > widthRatio && scaleRatio > heightRatio) {
    return scaleRatio;
  }

  if (widthRatio > scaleRatio && widthRatio > heightRatio) {
    return widthRatio;
  }

  return heightRatio;
}

export default function drawMaps(context, maps, view) {
  maps
    .filter(({ isActive }) => isActive())
    .forEach((map) => drawMap(context, map, view));
}
