import useState from "../state/useState.mjs";
import useStates from "../state/useStates.mjs";

export const [maps, setMaps] = useState([]);

export const activeMaps = maps.flatMap((maps) =>
  useStates(...maps.map(({ isActive }) => isActive)).map(() =>
    maps.filter(({ isActive }) => isActive()),
  ),
);
