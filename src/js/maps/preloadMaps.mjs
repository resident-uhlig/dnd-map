import fetchWithProgress from "../preloading/fetchWithProgress.mjs";
import { loadImage } from "../preloading/loadImage.mjs";
import useState from "../state/useState.mjs";
import convert from "../rules/convert.mjs";
import useStateArray from "../state/useStateArray.mjs";

function normalizeIsActive(maps) {
  return maps.map((map) => {
    const [isActive, setIsActive] = useState(map.isActive);
    map.isActive = isActive;
    map.setIsActive = setIsActive;
    return map;
  });
}

function normalizeScales(maps) {
  return maps.map((map) => {
    if (map.scale.ft) {
      map.scale = {
        mi: convert(map.scale.ft, "foot", "mile"),
        px: map.scale.px,
      };
    }

    map.scale.pxPerMi = map.scale.px / map.scale.mi;
    map.scale.miPerPx = map.scale.mi / map.scale.px;
    return map;
  });
}

function normalizeLandmarks(maps) {
  return maps.map((map) => {
    map.landmarks.unshift({
      type: "maps",
      x_mi: map.center.x_mi,
      y_mi: map.center.y_mi,
      name: map.name,
    });

    map.landmarks.forEach((landmark) => (landmark.map = map));
    [map.landmarks, map.setLandmarks] = useStateArray(map.landmarks);
    return map;
  });
}

function preloadImages(maps, setProgress) {
  return Promise.all(
    maps.map((map) =>
      loadImage(map.source, setProgress)
        .then((image) => processImage(map, image))
        .then((image) => {
          map.image = image;
          return map;
        }),
    ),
  );
}

function processImage({ crop }, image) {
  if (crop) {
    const width = image.width - crop.right - crop.left;
    const height = image.height - crop.bottom - crop.top;
    const canvas = new OffscreenCanvas(width, height);
    const context = canvas.getContext("2d");
    context.drawImage(
      image,
      crop.left,
      crop.top,
      width,
      height,
      0,
      0,
      width,
      height,
    );
    return canvas;
  }

  return image;
}

function normalizeDimensions(maps) {
  return maps.map((map) => {
    map.dimensions = {
      width_mi: map.image.width * map.scale.miPerPx,
      height_mi: map.image.height * map.scale.miPerPx,
    };

    return map;
  });
}

function sort(maps) {
  return maps.sort((a, b) => a.scale.pxPerMi - b.scale.pxPerMi);
}

export default function preloadMaps(setProgress) {
  return fetchWithProgress("data/maps.json", setProgress)
    .then((blob) => blob.text())
    .then(JSON.parse)
    .then(normalizeIsActive)
    .then(normalizeScales)
    .then(normalizeLandmarks)
    .then((maps) => preloadImages(maps, setProgress))
    .then(normalizeDimensions)
    .then(sort);
}
