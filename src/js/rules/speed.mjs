export default {
  fast: {
    name: "speed.fast",
    ftPerMin: 400,
    miPerHour: 4,
    miPerDay: 30,
  },
  normal: {
    name: "speed.normal",
    ftPerMin: 300,
    miPerHour: 3,
    miPerDay: 24,
  },
  slow: {
    name: "speed.slow",
    ftPerMin: 200,
    miPerHour: 2,
    miPerDay: 18,
  },
  camel: {
    name: "mounts.camel",
    ftPerRound: 50,
  },
  donkeyOrMule: {
    name: "mounts.donkey_or_mule",
    ftPerRound: 40,
  },
  elephant: {
    name: "mounts.elephant",
    ftPerRound: 40,
  },
  draftHorse: {
    name: "mounts.horse_draft",
    ftPerRound: 40,
  },
  ridingHorse: {
    name: "mounts.horse_riding",
    ftPerRound: 60,
  },
  mastiff: {
    name: "mounts.mastiff",
    ftPerRound: 40,
  },
  pony: {
    name: "mounts.pony",
    ftPerRound: 40,
  },
  warhorse: {
    name: "mounts.warhorse",
    ftPerRound: 60,
  },
  galley: {
    name: "vehicles.galley",
    miPerHour: 4,
  },
  keelboat: {
    name: "vehicles.keelboat",
    miPerHour: 1,
  },
  longship: {
    name: "vehicles.longship",
    miPerHour: 3,
  },
  rowboat: {
    name: "vehicles.rowboat",
    miPerHour: 1.5,
  },
  sailingShip: {
    name: "vehicles.sailing_ship",
    miPerHour: 2,
  },
  warship: {
    name: "vehicles.warship",
    miPerHour: 2.5,
  },
};
