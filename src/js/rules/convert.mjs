const CONVERSIONS = {
  mile: {
    foot: {
      /*

      1 mi = 1.5 km
      1 ft = 0.3 m
      1 km = 1000 m

      x ft = 1 mi = 1.5 km = 1500 m = 5000 ft

      */
      factor: 5000,
    },

    minute: {
      callback: (value, { miPerDay, miPerHour, ftPerMin }) => {
        const days = Math.floor(value / miPerDay);
        value = value % miPerDay;
        const hours = Math.floor(value / miPerHour);
        value = value % miPerHour;
        const minutes = Math.floor(convert(value, "mile", "foot") / ftPerMin);
        return (days * 24 + hours) * 60 + minutes;
      },
    },
  },
};

// calculate reverse
Object.entries(CONVERSIONS).forEach(([from, to]) => {
  Object.entries(to).forEach(([to, { factor, callback }]) => {
    if (!factor || callback) {
      return;
    }

    if (!CONVERSIONS[to]) {
      CONVERSIONS[to] = {};
    }

    CONVERSIONS[to][from] = { factor: 1 / factor };
  });
});

export default function convert(value, from, to, factor) {
  if (from === to) {
    return value;
  }

  if (factor === undefined) {
    factor = CONVERSIONS[from][to].factor;
  }

  const { callback } = CONVERSIONS[from][to];
  if (callback) {
    return callback(value, factor);
  }

  return value * factor;
}
