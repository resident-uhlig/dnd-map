function createTestList() {
  const ul = document.createElement("ul");
  ul.classList.add("tests");
  return ul;
}

function createCodeBlock(code) {
  const pre = document.createElement("pre");
  pre.classList.add("code");
  pre.append(code);
  return pre;
}

export default class HtmlReporter {
  #tests;
  #test;

  constructor(parent) {
    const link = document.createElement("a");
    const url = location.href.split("?")[0];
    const params = new URLSearchParams(location.search);
    params.delete("filter");
    link.href = `${url}?${params}`;
    link.append("(no filter)");

    this.#tests = createTestList();
    parent.append(link, this.#tests);
  }

  beginSection(name) {
    this.#test = document.createElement("li");
    this.#test.append(document.createTextNode(name));
    this.#tests.append(this.#test);

    this.#tests = createTestList();
    this.#test.append(this.#tests);
  }

  endSection() {
    this.#tests = this.#tests.parentNode.parentNode;
  }

  beginTest(name) {
    this.#test = document.createElement("li");

    const link = document.createElement("a");
    const url = location.href.split("?")[0];
    const params = new URLSearchParams(location.search);
    params.set("filter", name);
    link.href = `${url}?${params}`;
    link.append("(filter)");

    this.#test.append(document.createTextNode(name), " ", link, ": ");

    this.#tests.append(this.#test);
  }

  endAll() {
    const test = this.#tests.querySelector(".failure, .error");
    if (test) {
      // use timeout because otherwise Chrome browser ignores it after page reload
      setTimeout(() => test.scrollIntoView(true), 100);
    }
  }

  success() {
    this.#test.append(document.createTextNode("success"));
    this.#test.classList.add("success");
  }

  skip() {
    this.#test.append(document.createTextNode("skip"));
    this.#test.classList.add("skip");
  }

  failure(failure) {
    this.#test.append(
      document.createTextNode("failure"),
      createCodeBlock(failure.stack),
    );
    this.#test.classList.add("failure");
  }

  error(error) {
    this.#test.append(
      document.createTextNode("error"),
      createCodeBlock(error.stack),
    );
    this.#test.classList.add("error");
  }
}
