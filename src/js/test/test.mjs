import assert from "./assert.mjs";
import Failure from "./assert/Failure.mjs";

const root = [];
let tests = root;

function runTests(tests, reporter, filter) {
  assert(tests).isNotEmpty("no tests");
  tests.forEach(({ name, test, tests }) => {
    if (tests) {
      reporter.beginSection(name);
      tryCallback(() => runTests(tests, reporter, filter), reporter);
      reporter.endSection();
    }

    if (test) {
      reporter.beginTest(name);
      if (!filter || filter.test(name)) {
        tryCallback(() => {
          test();
          reporter.success();
        }, reporter);
      } else {
        reporter.skip();
      }
    }
  });
}

function tryCallback(callback, reporter) {
  try {
    callback();
  } catch (e) {
    if (e instanceof Failure) {
      reporter.failure(e);
    } else {
      reporter.error(e);
    }
  }
}

const consoleAssert = console.assert;

function consoleAssertWithThrow(assertion) {
  if (!assertion) {
    consoleAssert.apply(this, arguments);
    throw new Error("assertion failed");
  }
}

function beforeSuite() {
  console.assert = consoleAssertWithThrow;
}

function afterSuite() {
  console.assert = consoleAssert;
}

function getFilter() {
  const params = new URLSearchParams(location.search);
  const filter = params.get("filter");
  if (filter) {
    return new RegExp(filter);
  }
}

export default function run(reporter) {
  beforeSuite();
  try {
    runTests(root, reporter, getFilter());
    reporter.endAll();
  } catch (e) {
    afterSuite();
    alert(e);
  }
}

export function describe(name, description) {
  const parent = tests;
  tests = [];
  parent.push({ name, tests });
  description();
  tests = parent;
}

export function it(name, test) {
  tests.push({ name, test });
}
