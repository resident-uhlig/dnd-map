import ArrayAssert from "./assert/ArrayAssert.mjs";
import NodeAssert from "./assert/NodeAssert.mjs";
import Assert from "./assert/Assert.mjs";
import Failure from "./assert/Failure.mjs";
import StringAssert from "./assert/StringAssert.mjs";

export default function assert(actual) {
  if (Array.isArray(actual)) {
    return new ArrayAssert(actual);
  }

  if (actual instanceof Node) {
    return new NodeAssert(actual);
  }

  if (typeof actual === "string") {
    return new StringAssert(actual);
  }

  return new Assert(actual);
}

export function fail(message) {
  throw new Failure(message);
}
