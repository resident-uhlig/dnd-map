import Assert from "./Assert.mjs";
import assert from "../assert.mjs";

export default class StringAssert extends Assert {
  #actual;

  constructor(actual) {
    super(actual);
    this.#actual = actual;
  }

  trim() {
    return assert(this.#actual.trim());
  }
}
