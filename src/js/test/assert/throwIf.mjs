import Failure from "./Failure.mjs";

export function throwIfFalse(boolean, ...messages) {
  throwIfTrue(!boolean, ...messages);
}

export function throwIfTrue(boolean, ...messages) {
  if (boolean) {
    // filter out undefined messages
    throw new Failure(messages.filter((message) => message).join("\n"));
  }
}
