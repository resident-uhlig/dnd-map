import Assert from "./Assert.mjs";
import assert from "../assert.mjs";

// original from https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Whitespace#whitespace_helper_functions
function dataOf(node) {
  return node.textContent.replace(/[\t\n\r ]+/g, " ");
}

function normalizeTextNodes(parent) {
  if (parent.childNodes.length < 2) {
    return;
  }

  for (let i = 1; i < parent.childNodes.length; ) {
    const previous = parent.childNodes.item(i - 1);
    const current = parent.childNodes.item(i);
    if (
      previous.nodeType === Node.TEXT_NODE &&
      current.nodeType === Node.TEXT_NODE
    ) {
      previous.appendData(current.data);
      current.remove();
    } else {
      // only increase i when nothing has changed
      // this is to process the next element as well, when it has moved up to the current index after the old one has been removed
      i++;
    }
  }
}

export default class NodeAssert extends Assert {
  #actual;

  constructor(actual) {
    super(actual);
    this.#actual = actual;
  }

  innerHtml() {
    const div = document.createElement("div");
    div.append(this.#actual);
    return assert(div.innerHTML);
  }

  isSemanticallyEqualToString(string) {
    const template = document.createElement("template");
    template.innerHTML = string;
    this.isSemanticallyEqualToNode(template.content);
  }

  isSemanticallyEqualToNode(expected) {
    assert(this.#actual.nodeType).isStrictlyEqualTo(expected.nodeType);
    normalizeTextNodes(this.#actual);
    assert(Array.from(this.#actual.childNodes)).hasSameSizeAs(
      expected.childNodes,
      `'${this.#actual.innerHTML}'`,
      `'${expected.innerHTML}'`,
    );
    this.#actual.childNodes.forEach((node, index) => {
      assert(node).isSemanticallyEqualToNode(expected.childNodes.item(index));
    });

    // in order of https://developer.mozilla.org/en-US/docs/Web/API/Node/nodeType
    switch (this.#actual.nodeType) {
      case Node.ELEMENT_NODE:
        const attributes = Array.from(this.#actual.attributes);
        assert(attributes).hasSameSizeAs(expected.attributes);
        attributes.forEach((attribute, index) => {
          const other = expected.attributes.item(index);
          assert(attribute.name).isStrictlyEqualTo(other.name);
          assert(attribute.value).isStrictlyEqualTo(other.value);
        });
        break;

      case Node.TEXT_NODE:
      case Node.COMMENT_NODE:
        assert(dataOf(this.#actual)).isStrictlyEqualTo(dataOf(expected));
        break;

      case Node.DOCUMENT_FRAGMENT_NODE:
        // do nothing special
        break;

      default:
        throw new Error(`Node type '${this.#actual.nodeType}', not supported`);
    }
  }
}
