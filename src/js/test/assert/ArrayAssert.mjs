import Assert from "./Assert.mjs";
import { throwIfFalse, throwIfTrue } from "./throwIf.mjs";

export default class ArrayAssert extends Assert {
  #actual;

  constructor(actual) {
    super(actual);
    this.#actual = actual;
  }

  isNotEmpty(...messages) {
    throwIfTrue(this.#actual.length === 0, "Array is empty.", messages);
  }

  hasSameSizeAs(array, ...messages) {
    throwIfFalse(
      this.#actual.length === array.length,
      this.#actual,
      array,
      ...messages,
      `Actual size '${this.#actual.length}' !== expected size '${array.length}'.`,
    );
  }
}
