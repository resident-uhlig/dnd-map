import { throwIfFalse } from "./throwIf.mjs";

export default class Assert {
  #actual;

  constructor(actual) {
    this.#actual = actual;
  }

  isStrictlyEqualTo(expected) {
    throwIfFalse(
      this.#actual === expected,
      `Actual value '${this.#actual}' (${typeof this
        .#actual}) !== expected value '${expected}' (${typeof expected}).`,
    );
  }

  isTrue() {
    this.isStrictlyEqualTo(true, `'${this.#actual}' is not true`);
  }

  isFalse() {
    this.isStrictlyEqualTo(false, `'${this.#actual}' is not false`);
  }
}
