"use strict";

import "./html/html.test.mjs";
import "./state/useState.test.mjs";
import "./state/useStates.test.mjs";

import run from "./test/test.mjs";
import HtmlReporter from "./test/HtmlReporter.mjs";

document.addEventListener("DOMContentLoaded", () => {
  run(new HtmlReporter(document.body));
});
