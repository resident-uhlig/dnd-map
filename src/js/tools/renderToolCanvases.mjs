import renderViewCanvases from "./view/renderViewCanvases.mjs";
import html from "../html/html.mjs";
import renderShareCanvases from "./share/renderShareCanvases.mjs";
import renderMeasureCanvas from "./measure/renderMeasureCanvas.mjs";
import renderLandmarksCanvas from "./landmarks/renderLandmarksCanvas.mjs";
import renderAnnotateCanvas from "./annotate/renderAnnotateCanvas.mjs";

export default function renderToolCanvases() {
  return html`${renderLandmarksCanvas()} ${renderViewCanvases()}
  ${renderAnnotateCanvas()} ${renderShareCanvases()} ${renderMeasureCanvas()}`;
}
