import i18n from "../i18n.mjs";
import html from "../html/html.mjs";

function isBindingSupported(binding) {
  return (
    "ontouchstart" in window === (binding.event === "touchmove") ||
    binding.event === "click" ||
    binding.event === "contextmenu"
  );
}

function renderBinding({
  event,
  touches,
  buttons,
  deltaY,
  shift,
  control,
  alt,
  description,
}) {
  const keys = [];
  switch (event) {
    case "mousemove":
      switch (buttons) {
        case 1:
          keys.push(i18n("keybindings.mousedown"));
          break;

        default:
          console.warn(`unmapped mousemove buttons ${buttons}`);
      }

      keys.push(i18n("keybindings.mousemove"));
      break;

    case "touchmove":
      keys.push(i18n("keybindings.touchmove")(touches));
      break;

    case "mousedown":
      switch (buttons) {
        case 1:
          keys.push(i18n("keybindings.mousedown"));
          break;

        default:
          console.warn(`unmapped mousedown buttons ${buttons}`);
      }
      break;

    case "click":
      keys.push(i18n("keybindings.mousedown"));
      break;

    case "wheel":
      if (deltaY) {
        keys.push(i18n("keybindings.wheel"));
      } else {
        console.warn("unmapped wheel key binding");
      }
      break;
  }

  if (shift) {
    keys.push(i18n("keybindings.keys.Shift"));
  }

  if (control) {
    keys.push(i18n("keybindings.keys.Control"));
  }

  if (alt) {
    keys.push(i18n("keybindings.keys.Alt"));
  }

  return html`
    <li>
      ${keys.map(
        (key, index) => html`${index === 0 ? "" : " + "}<kbd>${key}</kbd>`,
      )}
      ${description}
    </li>
  `;
}

export default function renderKeyBindings(isActive, bindings) {
  bindings = bindings
    .filter(({ description }) => description)
    .filter(isBindingSupported);

  return isActive.map((isActive) => {
    if (isActive) {
      return bindings.map(renderBinding);
    }
  });
}
