import renderCopyPositionCanvas from "./copy/renderCopyPositionCanvas.mjs";

export default function renderShareCanvases() {
  return renderCopyPositionCanvas();
}
