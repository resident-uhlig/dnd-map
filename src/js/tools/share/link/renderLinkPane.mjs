import renderToolPane from "../../renderToolPane.mjs";
import i18n from "../../../i18n.mjs";
import {isActive} from "./renderLinkToolbar.mjs";
import html from "../../../html/html.mjs";
import view from "../../../maps/view.mjs";

function copyLink({ x_mi, y_mi, pxPerMi }) {
  const params = new URLSearchParams(location.search);
  params.set("x_mi", x_mi);
  params.set("y_mi", y_mi);
  params.set("px_per_mi", pxPerMi);
  const url = `${location.href.split("?")[0]}?${params.toString()}`;
  // Unfortunately we can't use navigator.share here because the copy link button in Windows 10 UI on the developer's PC does nothing.
  navigator.clipboard.writeText(url).then(() => {});
}

export default function renderLinkPane() {
  return renderToolPane(
    isActive,
    i18n("tools.share"),
    () => html`
      <button
        type="button"
        onclick="${() => copyLink(view())}"
      >
        ${i18n("tools.share.share_link")}
      </button>
    `
  );
}
