import useState from "../../../state/useState.mjs";
import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderLinkToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/share-solid.svg",
    title: i18n("tools.share"),
    setIsActive,
  });
}
