import createFullsizeCanvas from "../../../canvas/createFullsizeCanvas.mjs";
import { isActive } from "./renderCopyPositionToolbar.mjs";
import { position } from "./renderCopyPositionPane.mjs";
import useStates from "../../../state/useStates.mjs";
import view from "../../../maps/view.mjs";
import drawPoint from "./drawPoint.mjs";

const START_COLOR = { r: 0xd3, g: 0x24, b: 0x30, a: 1 };

function fadeOut(onAnimationFrame, { durationMillis }) {
  let request;
  const cancel = () => {
    if (request) {
      cancelAnimationFrame(request);
      request = undefined;
    }
  };

  const startMillis = new Date().getTime();
  function next(resolve) {
    request = requestAnimationFrame(() => {
      const progress = Math.max(
        0,
        1 - (new Date().getTime() - startMillis) / durationMillis,
      );
      onAnimationFrame(progress);
      if (progress > 0) {
        next(resolve);
      } else {
        resolve();
      }
    });
  }

  const promise = new Promise(next);

  return [cancel, promise];
}

export default function renderCopyPositionCanvas() {
  let fillColor = START_COLOR;
  const [canvas, requestDraw] = createFullsizeCanvas((context) =>
    drawPoint(context, fillColor),
  );

  const context = canvas.getContext("2d");
  let cancelFadeOut, fadeOutPromise;
  position(() => {
    if (cancelFadeOut) {
      cancelFadeOut();
    }

    fillColor = START_COLOR;
    [cancelFadeOut, fadeOutPromise] = fadeOut(
      (progress) => {
        fillColor = { ...fillColor, a: fillColor.a * progress };
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawPoint(context, fillColor);
      },
      { durationMillis: 350 },
    );

    fadeOutPromise.then(() => (cancelFadeOut = undefined));
  });

  useStates(view, isActive)(requestDraw);
  return canvas;
}
