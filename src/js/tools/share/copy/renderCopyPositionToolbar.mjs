import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderCopyPositionToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/location-crosshairs-solid.svg",
    title: i18n("tools.get_position"),
    accessKey: "8",
    setIsActive,
  });
}
