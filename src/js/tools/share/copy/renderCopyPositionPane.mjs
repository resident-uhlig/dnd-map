import renderToolPane from "../../renderToolPane.mjs";
import { isActive } from "./renderCopyPositionToolbar.mjs";
import html from "../../../html/html.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

export const [position, setPosition] = useState({ x_mi: 0, y_mi: 0 });

function copyPosition(position) {
  navigator.clipboard.writeText(JSON.stringify(position));
}

export default function renderCopyPositionPane() {
  return renderToolPane(
    isActive,
    i18n("tools.get_position"),
    () => html`
      <table>
        <tr>
          <th>X:</th>
          <td>
            ${position.map(({ x_mi }) =>
              i18n("format")(x_mi, "mile", "narrow"),
            )}
          </td>
        </tr>
        <tr>
          <th>Y:</th>
          <td>
            ${position.map(({ y_mi }) =>
              i18n("format")(y_mi, "mile", "narrow"),
            )}
          </td>
        </tr>
      </table>

      <button type="button" onclick="${() => copyPosition(position())}">
        ${i18n("tools.get_position.click")}
      </button>
    `,
  );
}
