import { isActive } from "./renderCopyPositionToolbar.mjs";
import calculatePositionOnCanvas from "../../../canvas/calculatePositionOnCanvas.mjs";
import view from "../../../maps/view.mjs";
import { position } from "./renderCopyPositionPane.mjs";
import drawFilledCircle from "../../../canvas/drawFilledCircle.mjs";
import rgba from "../../../css/rgba.mjs";
import drawHollowCircle from "../../../canvas/drawHollowCircle.mjs";
import getCssProperty from "../../../css/getCssProperty.mjs";

export default function drawPoint(context, fillColor) {
  if (isActive()) {
    const xy = calculatePositionOnCanvas(view(), position());
    drawFilledCircle(context, xy, 15, {
      color: rgba(fillColor),
    });
    drawHollowCircle(context, xy, 15, {
      color: getCssProperty("--active-color"),
      lineWidth: 2,
    });
  }
}
