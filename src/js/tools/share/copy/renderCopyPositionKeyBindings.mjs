import registerKeyBindings from "../../registerKeyBindings.mjs";
import renderKeyBindings from "../../renderKeyBindings.mjs";
import { isActive } from "./renderCopyPositionToolbar.mjs";
import { setPosition } from "./renderCopyPositionPane.mjs";
import calculatePositionOnMap from "../../../canvas/calculatePositionOnMap.mjs";
import i18n from "../../../i18n.mjs";
import view from "../../../maps/view.mjs";

export default function renderCopyPositionKeyBindings() {
  const bindings = [
    {
      description: i18n("tools.get_position"),
      event: "click",
      callback: (event) => setPosition(calculatePositionOnMap(view(), event)),
    },
    {
      priority: 1,
      description: i18n("tools.get_position"),
      event: "mousemove",
      buttons: 1,
      callback: (event) => {
        setPosition(calculatePositionOnMap(view(), event));
        return false;
      },
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
