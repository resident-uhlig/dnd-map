import renderToolPane from "../../renderToolPane.mjs";
import i18n from "../../../i18n.mjs";
import html from "../../../html/html.mjs";
import {isActive} from "./renderNetworkToolbar.mjs";
import {
  isSyncActive,
  requestView,
  sendHello,
  setIsSyncActive
} from "./messages.mjs";

function openNewInstance() {
  window.open(location.href, "_blank");
}

function changeSyncActive({target: {checked}}) {
  setIsSyncActive(checked);
  sendHello();
  requestView();
}

export default function renderNetworkPane() {
  return renderToolPane(
    isActive,
    i18n("tools.network"),
    () => html`
      <button
        type="button"
        onclick="${openNewInstance}"
      >
        ${i18n("tools.network.new_instance")}
      </button>

      <label>
        <input
          type="checkbox"
          checked="${isSyncActive}"
          onchange="${changeSyncActive}"
        >
        ${i18n("tools.network.sync_view")}
      </label>
    `
  );
}
