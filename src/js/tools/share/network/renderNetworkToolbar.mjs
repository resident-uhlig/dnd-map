import useState from "../../../state/useState.mjs";
import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";
import {sendHello} from "./messages.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

sendHello();

export default function renderNetworkToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/network-wired-solid.svg",
    title: i18n("tools.network"),
    setIsActive,
  });
}
