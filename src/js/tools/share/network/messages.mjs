import view, { goto } from "../../../maps/view.mjs";
import useState from "../../../state/useState.mjs";
import { position, setPosition } from "../copy/renderCopyPositionPane.mjs";

export const [isSyncActive, setIsSyncActive] = useState(false);

const peers = window.opener ? [window.opener] : [];

window.addEventListener("message", ({ source, data: { type, data } }) =>
  handleMessage(source, type, data),
);

view((view) => {
  if (!isSyncActive()) {
    sendView(peers, view);
  }
});

function handleMessage(source, type, data) {
  switch (type) {
    case "hello":
      if (!peers.includes(source)) {
        peers.push(source);
      }

      break;

    case "view":
      if (!isSyncActive()) {
        sendView([source], view());
      }
      break;

    case "setView":
      if (isSyncActive()) {
        goto(data);
      }

      break;

    case "setPosition":
      if (isSyncActive()) {
        setPosition(data);
      }

      break;
  }
}

function sendView(destinations, view) {
  sendMessage(destinations, "setView", view);
}

position((position) => {
  if (!isSyncActive()) {
    sendMessage(peers, "setPosition", position);
  }
});

export function requestView() {
  sendMessage(peers, "view");
}

export function sendHello() {
  sendMessage(peers, "hello");
}

function sendMessage(destinations, type, data) {
  destinations
    .filter((destination) => destination)
    .forEach((destination) => destination.postMessage({ type, data }));
}
