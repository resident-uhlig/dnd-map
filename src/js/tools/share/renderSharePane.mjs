import renderCopyPositionPane from "./copy/renderCopyPositionPane.mjs";
import html from "../../html/html.mjs";
import renderNetworkPane from "./network/renderNetworkPane.mjs";
import renderLinkToolbar from "./link/renderLinkToolbar.mjs";
import renderLinkPane from "./link/renderLinkPane.mjs";

export default function renderSharePane() {
  return html`
    ${renderCopyPositionPane()}
    ${renderLinkPane()}
    ${renderNetworkPane()}
  `;
}
