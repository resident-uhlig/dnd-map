import renderCopyPositionToolbar from "././copy/renderCopyPositionToolbar.mjs";
import html from "../../html/html.mjs";
import renderNetworkToolbar from "./network/renderNetworkToolbar.mjs";
import renderToolBar from "../renderToolBar.mjs";
import renderLinkToolbar from "./link/renderLinkToolbar.mjs";

export default function renderShareToolbar() {
  return renderToolBar(
    () => html`
      <li>${renderCopyPositionToolbar()}</li>
      <li>${renderLinkToolbar()}</li>
      <li>${renderNetworkToolbar()}</li>
    `,
  );
}
