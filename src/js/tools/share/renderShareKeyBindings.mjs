import renderCopyPositionKeyBindings from "./copy/renderCopyPositionKeyBindings.mjs";

export default function renderShareKeyBindings() {
  return renderCopyPositionKeyBindings();
}
