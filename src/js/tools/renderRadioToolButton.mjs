import renderToolButton from "./renderToolButton.mjs";

export default function renderRadioToolButton({
  icon,
  title,
  name,
  value,
  common,
  accessKey,
  setIsActive,
}) {
  console.assert(setIsActive !== undefined);
  return renderToolButton({
    icon,
    title,
    name,
    value,
    type: "radio",
    accessKey,
    setIsActive,
    onClick: ({ target }) => {
      target.checked = common.setIsActive !== setIsActive;
      if (common.setIsActive) {
        common.setIsActive(false);
      }

      common.setIsActive = target.checked ? setIsActive : undefined;
    },
  });
}
