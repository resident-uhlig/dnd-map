import html from "../html/html.mjs";

export default function renderToolButton({
  icon,
  title,
  classes,
  checked,
  onClick,
  type,
  name,
  value,
  accessKey,
  setIsActive,
}) {
  const style = {};
  if (icon) {
    if (typeof icon === "function") {
      style.backgroundImage = icon.map((icon) => `url(${icon})`);
    } else {
      style.backgroundImage = `url(${icon})`;
    }
  }

  classes = classes || [];
  classes.push("tool-button");

  if (accessKey !== undefined) {
    if (typeof title === "function") {
      title = title.map((title) => `${title} [${accessKey}]`);
    } else {
      title = `${title} [${accessKey}]`;
    }
  }

  if (setIsActive) {
    onClick = [onClick, ({ target: { checked } }) => setIsActive(checked)];
  }

  return html`<input
    class="${classes.join(" ")}"
    title="${title}"
    type="${type || "checkbox"}"
    style="${style}"
    checked="${checked}"
    onclick="${onClick}"
    name="${name}"
    value="${value}"
    accesskey="${accessKey}"
  />`;
}
