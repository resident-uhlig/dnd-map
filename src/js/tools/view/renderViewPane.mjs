import html from "../../html/html.mjs";
import renderGridPane from "./grid/renderGridPane.mjs";
import renderLayersPane from "./layers/renderLayersPane.mjs";

export default function renderViewPane() {
  return html` ${renderGridPane()} ${renderLayersPane()} `;
}
