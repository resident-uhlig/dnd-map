import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

const [isActive, setIsActive] = useState(true);
export { isActive };

export default function renderMoveToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/arrows-up-down-left-right-solid.svg",
    title: i18n("tools.move_view"),
    checked: true,
    accessKey: "m",
    setIsActive,
  });
}
