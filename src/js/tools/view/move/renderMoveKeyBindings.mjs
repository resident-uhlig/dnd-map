import i18n from "../../../i18n.mjs";
import registerKeyBindings from "../../registerKeyBindings.mjs";
import {isActive} from "./renderMoveToolbar.mjs";
import renderKeyBindings from "../../renderKeyBindings.mjs";
import calculatePositionOnMap from "../../../canvas/calculatePositionOnMap.mjs";
import {setView} from "../../../maps/view.mjs";

const ZOOM_FACTOR = 1.05;

export default function renderMoveKeyBindings() {
  const bindings = [
    {
      event: "wheel",
      deltaY: true,
      description: i18n("tools.move_view.wheel"),
      callback: ({ deltaY, clientX, clientY }) => {
        setView((view) => {
          // keep the same distance from mouse to center in px (see offsetX_px)
          // this means the miles from mouse to center change (see return statement)
          const { x_mi, y_mi } = calculatePositionOnMap(view, {
            clientX,
            clientY,
          });
          const offsetX_px = (x_mi - view.x_mi) * view.pxPerMi;
          const offsetY_px = (y_mi - view.y_mi) * view.pxPerMi;

          // new scale
          const pxPerMi =
            deltaY > 0
              ? view.pxPerMi / ZOOM_FACTOR
              : view.pxPerMi * ZOOM_FACTOR;

          return {
            ...view,
            x_mi: x_mi - offsetX_px / pxPerMi,
            y_mi: y_mi - offsetY_px / pxPerMi,
            pxPerMi,
          };
        });
      },
    },
    {
      event: "mousemove",
      buttons: 1,
      description: i18n("tools.move_view.mousedown"),
      callback: ({ movementX, movementY }) => {
        setView((view) => {
          return {
            ...view,
            x_mi: view.x_mi - movementX / view.pxPerMi,
            y_mi: view.y_mi - movementY / view.pxPerMi,
          };
        });
      },
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
