import html from "../../html/html.mjs";
import renderToolBar from "../renderToolBar.mjs";
import renderFullscreenToolbar from "./fullscreen/renderFullscreenToolbar.mjs";
import renderMoveToolbar from "./move/renderMoveToolbar.mjs";
import renderGridToolbar from "./grid/renderGridToolbar.mjs";
import renderLayersToolbar from "./layers/renderLayersToolbar.mjs";

export default function renderViewToolbar() {
  return renderToolBar(
    () => html`
      <li>${renderMoveToolbar()}</li>
      <li>${renderGridToolbar()}</li>
      <li>${renderLayersToolbar()}</li>
      <li>${renderFullscreenToolbar()}</li>
    `,
  );
}
