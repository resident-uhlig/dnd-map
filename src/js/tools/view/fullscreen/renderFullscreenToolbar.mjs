import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";
import { isFullscreen, requestFullscreen } from "./fullscreen.mjs";

export default function renderFullscreenToolbar() {
  return renderToolButton({
    icon: isFullscreen.map((isFullscreen) =>
      isFullscreen
        ? "css/fontawesome/compress-solid.svg"
        : "css/fontawesome/expand-solid.svg",
    ),
    title: isFullscreen.map((isFullscreen) =>
      isFullscreen
        ? i18n("tools.fullscreen.deactivate")
        : i18n("tools.fullscreen"),
    ),
    accessKey: "f",
    checked: isFullscreen,
    onClick: ({ target: checked }) => requestFullscreen(checked),
  });
}
