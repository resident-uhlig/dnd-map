import useState from "../../../state/useState.mjs";

const [isFullscreen, setIsFullscreen] = useState(!!document.fullscreenElement);

document.body.addEventListener("fullscreenchange", () => {
  setIsFullscreen(!!document.fullscreenElement);
});

function requestFullscreen(value) {
  if (document.fullscreenElement) {
    document.exitFullscreen();
  }

  if (value) {
    document.body.requestFullscreen();
  }
}

export { isFullscreen, requestFullscreen };
