import renderGridCanvas from "./grid/renderGridCanvas.mjs";

export default function renderViewCanvases() {
  return renderGridCanvas();
}
