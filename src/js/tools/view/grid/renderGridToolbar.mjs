import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderGridToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/table-cells-solid.svg",
    title: i18n("tools.show_grid"),
    accessKey: "g",
    setIsActive,
  });
}
