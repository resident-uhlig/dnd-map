import html from "../../../html/html.mjs";
import renderToolPane from "../../renderToolPane.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";
import { isActive } from "./renderGridToolbar.mjs";
import convert from "../../../rules/convert.mjs";
import useStates from "../../../state/useStates.mjs";
import view from "../../../maps/view.mjs";

const MIN_DISTANCE_MI = convert(1, "foot", "mile");
const MAX_DISTANCE_MI = 600;

const [shape, setShape] = useState("auto");
const [distance, setDistance] = useState(MAX_DISTANCE_MI);
const [unit, setUnit] = useState("mile");

const MIN_DISTANCE_PX = 20;

// DMG p. 242
const SCALES = [
  // 1.5 m / 5 ft
  {
    shape: "square",
    distance_mi: convert(5, "foot", "mile"),
  },
  // 3 m / 10 ft
  {
    shape: "square",
    distance_mi: convert(10, "foot", "mile"),
  },
  // 30 m / 100 ft
  {
    shape: "square",
    distance_mi: convert(100, "foot", "mile"),
  },
  // 1.5 km / 1 mi
  {
    shape: "hex",
    distance_mi: 1,
  },
  // 10 km / 6 mi
  {
    shape: "hex",
    distance_mi: 6,
  },
];

SCALES.sort((a, b) => {
  return a.distance_mi - b.distance_mi;
});

export const scale = useStates(shape, distance, unit, view).map(
  ([shape, distance, unit, { pxPerMi }]) => {
    if (shape === "auto") {
      for (const { shape, distance_mi } of SCALES) {
        const distance_px = distance_mi * pxPerMi;
        if (distance_px > MIN_DISTANCE_PX) {
          return {
            shape,
            distance_mi,
            distance_px,
          };
        }
      }

      return;
    }

    const [distance_mi] = limitDistance(
      convert(distance, unit, "mile"),
      "mile",
    );
    const distance_px = distance_mi * pxPerMi;
    if (distance_px > MIN_DISTANCE_PX) {
      return {
        shape,
        distance_mi,
        distance_px,
      };
    }
  },
);

function renderShape(value, shape, setShape) {
  return html`
    <li>
      <label>
        <input
          type="radio"
          name="grid-shape"
          value="${value}"
          checked="${shape.map((shape) => shape === value)}"
          onchange="${() => setShape(value)}"
        />
        ${i18n(`tools.show_grid.shape.${value}`)}
        ${value === "auto" ? html`(${renderAutoScale()})` : ""}
      </label>
    </li>
  `;
}

function renderUnit(value, unit) {
  return html` <option
    value="${value}"
    selected="${unit.map((unit) => unit === value)}"
  >
    ${i18n(`tools.show_grid.distance.unit.${value}`)}
  </option>`;
}

export function limitDistance(distance, unit) {
  const min = convert(MIN_DISTANCE_MI, "mile", unit);
  const max = convert(MAX_DISTANCE_MI, "mile", unit);
  const value = Math.round(Math.min(Math.max(min, distance), max) / min) * min;
  return [value, max];
}

function renderAutoScale() {
  return useStates(shape, scale).map(([shape, scale]) => {
    if (!scale || shape !== "auto") {
      return i18n("tools.show_grid.too_far_away");
    }

    return i18n("format")(scale.distance_mi, "mile");
  });
}

export default function renderGridPane() {
  const [maxDistance, setMaxDistance] = useState(MAX_DISTANCE_MI);

  unit((unit, oldUnit) => {
    if (oldUnit === undefined) {
      return;
    }

    const [value, max] = limitDistance(
      convert(distance(), oldUnit, unit),
      unit,
    );
    setMaxDistance(Math.round(max));
    setDistance(value);
  });

  return renderToolPane(
    isActive,
    i18n("tools.show_grid"),
    () => html`
      <ul class="checkboxes">
        ${renderShape("auto", shape, setShape)}
        ${renderShape("hex", shape, setShape)}
        ${renderShape("square", shape, setShape)}
      </ul>

      <label>
        <input
          type="number"
          min="1"
          step="1"
          max="${maxDistance}"
          value="${distance}"
          disabled="${shape.map((shape) => shape === "auto")}"
          oninput="${({ target: { value } }) => setDistance(value)}"
        />
        <select
          disabled="${shape.map((shape) => shape === "auto")}"
          oninput="${({ target: { value } }) => setUnit(value)}"
        >
          ${renderUnit("foot", unit)} ${renderUnit("mile", unit)}
        </select>

        ${i18n("tools.show_grid.distance")}
      </label>
    `,
  );
}
