import getCssProperty from "../../../css/getCssProperty.mjs";
import drawLine from "../../../canvas/drawLine.mjs";

function drawSquareGrid(
  context,
  { distance_px },
  { x_mi, y_mi, pxPerMi },
  style,
) {
  const width = context.canvas.width;
  const height = context.canvas.height;

  const x0 = distance_px - ((x_mi * pxPerMi - width / 2) % distance_px);
  const y0 = distance_px - ((y_mi * pxPerMi - height / 2) % distance_px);

  for (let x = x0; x < width; x += distance_px) {
    drawLine(context, { x, y: 0 }, { x, y: height }, style);
  }

  for (let y = y0; y < height; y += distance_px) {
    drawLine(context, { x: 0, y }, { x: width, y }, style);
  }
}

function drawHexGrid(
  context,
  { distance_mi, distance_px },
  { x_mi, y_mi, pxPerMi },
  style,
) {
  const width_px = context.canvas.width;
  const height_px = context.canvas.height;

  const width_mi = width_px / pxPerMi;
  const height_mi = height_px / pxPerMi;

  // this helped a lot for the formulas: https://www.redblobgames.com/grids/hexagons/
  const stepX_mi = distance_mi / 2;
  const x0 = (-(x_mi - width_mi / 2) % distance_mi) * pxPerMi;
  const numLinesX = Math.ceil(width_mi / stepX_mi) + 1;

  const size_mi = distance_mi / Math.sqrt(3);
  const stepY_mi = size_mi * 3;
  const offsetY_px = ((size_mi * 3) / 2) * pxPerMi;
  const numLinesY = Math.ceil(height_mi / stepY_mi) + 1;
  const y0 = (-(y_mi - height_mi / 2) % stepY_mi) * pxPerMi;

  const stepX_px = stepX_mi * pxPerMi;
  const stepY_px = stepY_mi * pxPerMi;
  const size_px = size_mi * pxPerMi;

  for (let i = 0; i <= numLinesX; i++) {
    const x = x0 + i * stepX_px;
    for (let j = 0; j <= numLinesY; j++) {
      const y = y0 + j * stepY_px + (i % 2) * offsetY_px;

      // vertical line
      drawLine(context, { x, y }, { x, y: y + size_px }, style);

      // diagonal line 1
      drawLine(
        context,
        { x, y },
        {
          x: x + distance_px / 2,
          y: y - size_px / 2,
        },
        style,
      );

      // diagonal line 2
      drawLine(
        context,
        { x, y: y + size_px },
        {
          x: x + distance_px / 2,
          y: y + (size_px * 3) / 2,
        },
        style,
      );
    }
  }
}

export default function drawGrid(context, scale, view) {
  if (!scale) {
    return;
  }

  const style = {
    lineWidth_px: getCssProperty("-grid-width"),
    color: getCssProperty("--grid-color"),
  };

  switch (scale.shape) {
    case "square":
      drawSquareGrid(context, scale, view, style);
      break;
    case "hex":
      drawHexGrid(context, scale, view, style);
      break;
  }
}
