import { isActive } from "./renderGridToolbar.mjs";
import { scale } from "./renderGridPane.mjs";
import drawGrid from "./drawGrid.mjs";
import createFullsizeCanvas from "../../../canvas/createFullsizeCanvas.mjs";
import useStates from "../../../state/useStates.mjs";
import view from "../../../maps/view.mjs";

export default function renderGridCanvas() {
  const [canvas, requestDraw] = createFullsizeCanvas((context) => {
    if (isActive()) {
      drawGrid(context, scale(), view());
    }
  });

  useStates(scale, view, isActive)(requestDraw);
  return canvas;
}
