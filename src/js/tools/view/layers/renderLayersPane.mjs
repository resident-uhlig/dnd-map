import renderToolPane from "../../renderToolPane.mjs";
import i18n from "../../../i18n.mjs";
import html from "../../../html/html.mjs";
import useState from "../../../state/useState.mjs";
import { isActive } from "./renderLayersToolbar.mjs";
import { maps } from "../../../maps/maps.mjs";
import useStates from "../../../state/useStates.mjs";

function renderLayer({ name, isActive, setIsActive }) {
  return html`
    <li>
      <label>
        <input
          type="checkbox"
          checked="${isActive}"
          onclick="${({ target: { checked } }) => setIsActive(checked)}"
        />
        ${i18n(name)}
      </label>
    </li>
  `;
}

const areAllActive = maps.flatMap((maps) =>
  useStates(...maps.map(({ isActive }) => isActive)).map(() =>
    maps.every(({ isActive }) => isActive()),
  ),
);

export default function renderLayersPane() {
  return renderToolPane(
    isActive,
    i18n("tools.control_layers_visibility"),
    () =>
      html`<ul class="checkboxes">
        <li>
          <label>
            <input
              type="checkbox"
              checked="${areAllActive}"
              onclick="${({ target: { checked } }) => {
                maps().forEach(({ setIsActive }) => setIsActive(checked));
              }}"
            />
            ${i18n("tools.control_layers_visibility.all")}
          </label>
        </li>
        ${maps.map((maps) => maps.map(renderLayer))}
      </ul>`,
  );
}
