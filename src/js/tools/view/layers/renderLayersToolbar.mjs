import renderToolButton from "../../renderToolButton.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderLayersToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/layer-group-solid.svg",
    title: i18n("tools.control_layers_visibility"),
    accessKey: "7",
    setIsActive,
  });
}
