import html from "../html/html.mjs";
import useState from ".././state/useState.mjs";
import renderExpandCollapseButton from "../html/renderExpandCollapseButton.mjs";

export default function renderToolBar(renderContent, isInitiallyExpanded) {
  const [isExpanded, setIsExpanded] = useState(isInitiallyExpanded);

  const classes = isExpanded.map((isExpanded) => {
    const classes = ["tool-bar"];
    if (isExpanded) {
      classes.push("expanded");
    }

    return classes.join(" ");
  });

  return html`
    <menu class="${classes}">
      ${renderContent()}
      <li>${renderExpandCollapseButton(isExpanded, setIsExpanded)}</li>
    </menu>
  `;
}
