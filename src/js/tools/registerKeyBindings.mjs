let touchesCache = {};
const bindingsByEvent = {};

document.addEventListener("touchstart", ({ touches }) => {
  updateTouchesCache(touches);
});

document.addEventListener("touchend", ({ touches }) => {
  updateTouchesCache(touches);
});

document.addEventListener("touchmove", ({ target, touches }) => {
  switch (touches.length) {
    case 1:
      dispatchTouchMoveAsMouseMove(target, touches);
      break;

    case 2:
      dispatchTouchMoveAsWheel(target, touches);
      break;

    default:
      console.warn(`unhandled length of touches: ${touches.length}`);
  }

  updateTouchesCache(touches);
});

function dispatchTouchMoveAsMouseMove(target, touches) {
  const movements = Array.from(touches).map(
    ({ identifier, screenX, screenY, clientX, clientY }) => {
      const old = touchesCache[identifier];
      return {
        movementX: screenX - old.screenX,
        movementY: screenY - old.screenY,
        clientX,
        clientY,
      };
    },
  );

  const init = { ...movements[0], buttons: 1, bubbles: true };
  target.dispatchEvent(new MouseEvent("mousemove", init));
}

function dispatchTouchMoveAsWheel(target, touches) {
  const newDeltaX = Math.abs(touches[0].clientX - touches[1].clientX);
  const clientX = touches[0].clientX + newDeltaX / 2;

  const newDeltaY = Math.abs(touches[0].clientY - touches[1].clientY);
  const oldDeltaY = Math.abs(
    touchesCache[touches[0].identifier].clientY -
      touchesCache[touches[1].identifier].clientY,
  );
  const clientY =
    Math.min(touches[0].clientY, touches[1].clientY) + newDeltaY / 2;

  const init = {
    deltaY: oldDeltaY - newDeltaY,
    clientX,
    clientY,
    bubbles: true,
  };

  target.dispatchEvent(new WheelEvent("wheel", init));
}

function updateTouchesCache(touches) {
  touchesCache = {};
  Array.from(touches).forEach(
    (touch) => (touchesCache[touch.identifier] = touch),
  );
}

function registerBinding(isActive, binding) {
  const { event } = binding;
  if (bindingsByEvent[event] === undefined) {
    document.addEventListener(event, forwardEvent);
    bindingsByEvent[event] = [];
  }

  bindingsByEvent[event].push({ isActive, priority: -1, ...binding });
}

function forwardEvent(event) {
  const { type } = event;
  const bindings = bindingsByEvent[type].filter((binding) =>
    testBinding(event, binding),
  );
  if (bindings === 0) {
    return;
  }

  // high number = handled first
  bindings.sort((a, b) => b.priority - a.priority);

  // break on first false
  const some = bindings.some(({ callback }) => false === callback(event));

  if (!some && bindings.length > 1) {
    console.warn(`${bindings.length} bindings for event ${type}`);
  }
}

function testBinding(event, { isActive, buttons, deltaY, touches }) {
  return (
    isActive() &&
    (deltaY === undefined || event.deltaY) &&
    (buttons === undefined || event.buttons === buttons) &&
    (touches === undefined || event.touches.length === touches) &&
    event.target instanceof HTMLCanvasElement
  );
}

export default function registerKeyBindings(isActive, bindings) {
  bindings.forEach((binding) => registerBinding(isActive, binding));
}
