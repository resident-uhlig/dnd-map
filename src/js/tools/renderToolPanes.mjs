import html from "../html/html.mjs";
import renderI18nPane from "./i18n/renderI18nPane.mjs";
import renderAnnotatePane from "./annotate/renderAnnotatePane.mjs";
import renderViewPane from "./view/renderViewPane.mjs";
import renderSharePane from "./share/renderSharePane.mjs";
import renderMeasurePane from "./measure/renderMeasurePane.mjs";
import renderLandmarksPane from "./landmarks/renderLandmarksPane.mjs";

export default function renderToolPanes() {
  return html`
    <dl class="tool-panes">
      ${renderViewPane()} ${renderSharePane()} ${renderMeasurePane()}
      ${renderLandmarksPane()} ${renderAnnotatePane()} ${renderI18nPane()}
    </dl>
  `;
}
