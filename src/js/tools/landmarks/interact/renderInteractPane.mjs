import renderToolPane from "../../renderToolPane.mjs";
import { isActive } from "./renderInteractToolbar.mjs";
import i18n from "../../../i18n.mjs";
import { hoveredLandmarks } from "./renderInteractKeyBindings.mjs";
import html from "../../../html/html.mjs";
import useState from "../../../state/useState.mjs";
import useStates from "../../../state/useStates.mjs";
import gotoLandmark from "./gotoLandmark.mjs";
import { SORTED_TYPES } from "../types.mjs";
import { visibleLandmarks } from "./visibleLandmarks.mjs";

const [typesFilter, setTypesFilter] = useState(["maps"]);
const [nameFilter, setNameFilter] = useState();

const filteredLandmarks = useStates(
  typesFilter,
  nameFilter,
  visibleLandmarks,
).map(([typesFilter, nameFilter, visibleLandmarks]) => {
  if (!nameFilter) {
    return undefined;
  }

  const pattern = new RegExp(nameFilter, "i");
  return visibleLandmarks
    .filter(({ type }) => typesFilter.includes(type))
    .filter(({ name }) => pattern.test(i18n(name)));
});

function renderHoveredLandmarks() {
  return hoveredLandmarks.map((landmarks) => {
    if (landmarks.length === 0) {
      return;
    }

    return html`
      ${i18n("tools.landmarks.interact.hover")}
      <dl>${landmarks.map(({ name }) => html`<dt>${i18n(name)}</dt>`)}</dl>
    `;
  });
}

function renderFilters() {
  return html` ${renderTypesFilter()} ${renderNameFilter()} `;
}

function renderTypesFilter() {
  return html`
    <ul class="checkboxes">
      ${SORTED_TYPES.map(
        ({ key, name }) => html`
          <li>
            <label>
              <input
                type="checkbox"
                checked="${typesFilter.map((filter) => filter.includes(key))}"
                onchange="${({ target: { checked } }) =>
                  setTypeFilter(key, checked)}"
              />
              ${name}
            </label>
          </li>
        `,
      )}
    </ul>
  `;
}

function setTypeFilter(type, checked) {
  setTypesFilter((types) => {
    if (checked) {
      if (types.includes(type)) {
        return types;
      }

      return [...types, type];
    }

    return types.filter((candidate) => candidate !== type);
  });
}

function renderNameFilter() {
  return html`
    <label>
      ${i18n("tools.landmarks.search.name")}
      <input
        type="text"
        required
        oninput="${({ target: { value } }) => setNameFilter(value)}"
      />
    </label>
  `;
}

function renderFilteredLandmarks() {
  return filteredLandmarks.map((landmarks) => {
    if (landmarks === undefined) {
      return;
    }

    if (landmarks.length === 0) {
      return i18n("tools.landmarks.search.no-results");
    }

    return html`<ul>
      ${landmarks.map(
        (landmark) => html`
          <li>
            <button type="button" onclick="${() => gotoLandmark(landmark)}">
              ${i18n("tools.landmarks.search.goto")(i18n(landmark.name))}
            </button>
          </li>
        `,
      )}
    </ul>`;
  });
}

export default function renderInteractPane() {
  return renderToolPane(
    isActive,
    i18n("tools.landmarks.interact"),
    () => html`
      ${renderHoveredLandmarks()} ${renderFilters()}
      ${renderFilteredLandmarks()}
    `,
  );
}
