import useState from "../../../state/useState.mjs";
import registerKeyBindings from "../../registerKeyBindings.mjs";
import renderKeyBindings from "../../renderKeyBindings.mjs";
import { isActive } from "./renderInteractToolbar.mjs";
import calculatePositionOnCanvas from "../../../canvas/calculatePositionOnCanvas.mjs";
import { ICON_SIZE } from "../drawLandmark.mjs";
import gotoLandmark from "./gotoLandmark.mjs";
import { visibleLandmarks } from "./visibleLandmarks.mjs";
import view from "../../../maps/view.mjs";

const [hoveredLandmarks, setHoveredLandmarks] = useState([]);
export { hoveredLandmarks };

function findHoveredLandmarks(view, landmarks, event) {
  return landmarks.filter((landmark) => collides(view, landmark, event));
}

function collides(view, landmark, { clientX, clientY }) {
  const { x, y } = calculatePositionOnCanvas(view, landmark);
  const x0 = x - ICON_SIZE;
  const y0 = y - ICON_SIZE;
  const x1 = x0 + ICON_SIZE * 2;
  const y1 = y0 + ICON_SIZE * 2;
  return clientX >= x0 && clientX <= x1 && clientY >= y0 && clientY <= y1;
}

export default function renderInteractKeyBindings() {
  const bindings = [
    {
      event: "mousemove",
      callback: (event) =>
        setHoveredLandmarks(
          findHoveredLandmarks(view(), visibleLandmarks(), event),
        ),
    },
    {
      event: "click",
      callback: () => {
        const landmarks = hoveredLandmarks();
        if (landmarks.length === 0) {
          return;
        }

        gotoLandmark(landmarks.shift());
      },
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
