import useState from "../../../state/useState.mjs";
import renderRadioToolButton from "../../renderRadioToolButton.mjs";
import i18n from "../../../i18n.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderInteractToolbar(common) {
  return renderRadioToolButton({
    icon: "css/fontawesome/hand-solid.svg",
    title: i18n("tools.landmarks.interact"),
    name: "landmarks-tool",
    value: "interact",
    common,
    accessKey: "9",
    setIsActive,
  });
}
