import { visibleLandmarks } from "./visibleLandmarks.mjs";

export default function registerRequestDrawInteract(requestDraw) {
  visibleLandmarks(requestDraw);
}
