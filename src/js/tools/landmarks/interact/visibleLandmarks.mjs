import { activeMaps } from "../../../maps/maps.mjs";
import useStates from "../../../state/useStates.mjs";
import { isActive } from "./renderInteractToolbar.mjs";
import useStaticState from "../../../state/useStaticState.mjs";

const activeLandmarks = activeMaps.flatMap((maps) => {
  if (maps.length === 0) {
    return useStaticState([]);
  }

  return useStates(...maps.map(({ landmarks }) => landmarks)).map((landmarks) =>
    landmarks.flatMap((landmarks) => landmarks),
  );
});

export const visibleLandmarks = useStates(isActive, activeLandmarks).map(
  ([isActive, activeLandmarks]) => (isActive ? activeLandmarks : []),
);
