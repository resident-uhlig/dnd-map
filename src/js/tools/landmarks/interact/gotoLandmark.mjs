import {goto} from "../../../maps/view.mjs";

export default function gotoLandmark({
  x_mi,
  y_mi,
  map: {
    scale: { pxPerMi },
  },
}) {
  goto({ x_mi, y_mi, pxPerMi });
}
