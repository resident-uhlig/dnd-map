import createFullsizeCanvas from "../../canvas/createFullsizeCanvas.mjs";
import drawInteractLandmarks from "./interact/drawInteractLandmarks.mjs";
import drawAddLandmarks from "./add/drawAddLandmarks.mjs";
import registerRequestDrawInteract from "./interact/registerRequestDrawInteract.mjs";
import registerRequestDrawAdd from "./add/registerRequestDrawAdd.mjs";
import view from "../../maps/view.mjs";

export default function renderLandmarksCanvas() {
  const [canvas, requestDraw] = createFullsizeCanvas((context) => {
    drawInteractLandmarks(context, view());
    drawAddLandmarks(context, view());
  });

  view(requestDraw);
  registerRequestDrawInteract(requestDraw);
  registerRequestDrawAdd(requestDraw);
  return canvas;
}
