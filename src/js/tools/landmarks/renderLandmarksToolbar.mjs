import html from "../../html/html.mjs";
import renderToolBar from "../renderToolBar.mjs";
import renderInteractToolbar from "./interact/renderInteractToolbar.mjs";
import renderAddToolbar from "./add/renderAddToolbar.mjs";

export default function renderLandmarksToolbar() {
  let common = {};
  return renderToolBar(
    () => html`
      <li>${renderInteractToolbar(common)}</li>
      <li>${renderAddToolbar(common)}</li>
    `,
  );
}
