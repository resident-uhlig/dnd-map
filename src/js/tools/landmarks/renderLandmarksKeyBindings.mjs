import renderInteractKeyBindings from "./interact/renderInteractKeyBindings.mjs";
import renderAddKeyBindings from "./add/renderAddKeyBindings.mjs";
import html from "../../html/html.mjs";

export default function renderLandmarksKeyBindings() {
  return html` ${renderInteractKeyBindings()} ${renderAddKeyBindings()} `;
}
