import i18n from "../../i18n.mjs";

const TYPES = {
  castles: {
    imageUrl: "css/fontawesome/fort-awesome.svg",
    canBeAddedByUser: true,
  },
  docks: {
    imageUrl: "css/fontawesome/anchor-solid.svg",
    canBeAddedByUser: true,
  },
  inns: {
    imageUrl: "css/fontawesome/beer-mug-empty-solid.svg",
    canBeAddedByUser: true,
  },
  islands: {
    imageUrl: "css/fontawesome/umbrella-beach-solid.svg",
    canBeAddedByUser: true,
  },
  maps: {
    imageUrl: "css/fontawesome/map-location-solid.svg",
    canBeAddedByUser: false,
  },
  pois: {
    imageUrl: "css/fontawesome/location-dot-solid.svg",
    canBeAddedByUser: true,
  },
  residences: {
    imageUrl: "css/fontawesome/house-solid.svg",
    canBeAddedByUser: true,
  },
  roads: { imageUrl: "css/fontawesome/road-solid.svg", canBeAddedByUser: true },
  shops: { imageUrl: "css/fontawesome/shop-solid.svg", canBeAddedByUser: true },
  temples: {
    imageUrl: "css/fontawesome/person-rays-solid.svg",
    canBeAddedByUser: true,
  },
  water: {
    imageUrl: "css/fontawesome/water-solid.svg",
    canBeAddedByUser: true,
  },
};

export default TYPES;

export const SORTED_TYPES = Object.entries(TYPES)
  .map(([key, type]) => ({
    key,
    ...type,
    name: i18n(`landmarks.types.${key}`),
  }))
  .sort((a, b) => a.name.localeCompare(b.name));
