import { loadImage } from "../../preloading/loadImage.mjs";
import TYPES from "./types.mjs";

function normalizeImage(image) {
  const { width, height } = image;
  if (width === height) {
    return image;
  }

  const size = Math.max(width, height);
  const canvas = new OffscreenCanvas(size, size);
  const context = canvas.getContext("2d");
  context.drawImage(image, (size - width) / 2, (size - height) / 2);
  return canvas;
}

export default function preloadLandmarkIcons(setProgress) {
  return Promise.all(
    Object.values(TYPES).map((type) =>
      loadImage(type.imageUrl, setProgress)
        .then(normalizeImage)
        .then((image) => {
          type.image = image;
          return type;
        }),
    ),
  );
}
