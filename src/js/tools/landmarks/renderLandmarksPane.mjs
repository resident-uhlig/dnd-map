import renderInteractPane from "./interact/renderInteractPane.mjs";
import renderAddPane from "./add/renderAddPane.mjs";
import html from "../../html/html.mjs";

export default function renderLandmarksPane() {
  return html` ${renderInteractPane()} ${renderAddPane()} `;
}
