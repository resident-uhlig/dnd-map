import useStates from "../../../state/useStates.mjs";
import { map } from "./renderAddPane.mjs";
import { isActive } from "./renderAddToolbar.mjs";
import useStaticState from "../../../state/useStaticState.mjs";

const activeLandmarks = map.flatMap((map) =>
  map ? map.landmarks : useStaticState([]),
);

export const visibleLandmarks = useStates(isActive, activeLandmarks).map(
  ([isActive, activeLandmarks]) => (isActive ? activeLandmarks : []),
);
