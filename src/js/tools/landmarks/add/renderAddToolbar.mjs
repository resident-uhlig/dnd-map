import useState from "../../../state/useState.mjs";
import renderRadioToolButton from "../../renderRadioToolButton.mjs";
import i18n from "../../../i18n.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderAddToolbar(common) {
  return renderRadioToolButton({
    icon: "css/fontawesome/plus-solid.svg",
    title: i18n("tools.landmarks.add"),
    name: "landmarks-tool",
    value: "add",
    common,
    setIsActive,
  });
}
