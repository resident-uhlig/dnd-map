import { visibleLandmarks } from "./visibleLandmarks.mjs";

export default function registerRequestDrawAdd(requestDraw) {
  visibleLandmarks(requestDraw);
}
