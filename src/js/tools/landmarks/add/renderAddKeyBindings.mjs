import i18n, { normalizeKey } from "../../../i18n.mjs";
import { map, name, setName, type } from "./renderAddPane.mjs";
import registerKeyBindings from "../../registerKeyBindings.mjs";
import renderKeyBindings from "../../renderKeyBindings.mjs";
import { isActive } from "./renderAddToolbar.mjs";
import calculatePositionOnMap from "../../../canvas/calculatePositionOnMap.mjs";
import { setI18n } from "../../../i18n/de_DE.mjs";
import view from "../../../maps/view.mjs";

function addLandmark(view, event, map, type, name) {
  if (!name) {
    alert(i18n(`tools.landmarks.add.name.required`));
    return;
  }

  const position = calculatePositionOnMap(view, event);
  const i18nKey = `${map.name}.${normalizeKey(name)}`;
  setI18n(i18nKey, name);
  const landmark = { ...position, type, name: i18nKey, map };
  map.setLandmarks.push(landmark);
  setName("");
}

export default function renderAddKeyBindings() {
  const bindings = [
    {
      description: i18n("tools.landmarks.add"),
      event: "click",
      callback: (event) => addLandmark(view(), event, map(), type(), name()),
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
