import drawLandmark from "../drawLandmark.mjs";
import { visibleLandmarks } from "./visibleLandmarks.mjs";

export default function drawAddLandmarks(context, view) {
  visibleLandmarks().forEach((landmark) =>
    drawLandmark(context, view, landmark),
  );
}
