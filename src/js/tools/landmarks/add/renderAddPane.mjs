import html from "../../../html/html.mjs";
import i18n from "../../../i18n.mjs";
import TYPES, { SORTED_TYPES } from "../types.mjs";
import useState from "../../../state/useState.mjs";
import renderToolPane from "../../renderToolPane.mjs";
import { isActive } from "./renderAddToolbar.mjs";
import { maps } from "../../../maps/maps.mjs";
import useStates from "../../../state/useStates.mjs";

const [mapIndex, setMapIndex] = useState(0);
const [type, setType] = useState(SORTED_TYPES[0].key);
const [name, setName] = useState("");

export { mapIndex, type, name, setName };

export const map = useStates(maps, mapIndex).map(
  ([maps, mapIndex]) => maps[mapIndex],
);

function renderMapSelect(maps) {
  return html`
    <label>
      ${i18n("tools.landmarks.add.map")}
      <select oninput="${({ target: { value } }) => setMapIndex(value)}">
        ${maps.map(renderMapOption)}
      </select>
    </label>
  `;
}

function renderMapOption({ name }, value) {
  return html` <option
    value="${value}"
    selected="${mapIndex.map((index) => index === value)}"
  >
    ${i18n(name)}
  </option>`;
}

function renderTypeSelect() {
  return html`
    <label>
      ${i18n("tools.landmarks.add.element-type")}
      <select oninput="${({ target: { value } }) => setType(value)}">
        ${SORTED_TYPES.filter(({ canBeAddedByUser }) => canBeAddedByUser).map(
          renderTypeOption,
        )}
      </select>
    </label>
  `;
}

function renderTypeOption({ key: value, name }) {
  return html` <option
    value="${value}"
    selected="${type.map((type) => type === value)}"
  >
    ${name}
  </option>`;
}

function renderNameInput() {
  return html`
    <label>
      ${i18n("tools.landmarks.add.name")}
      <input
        type="text"
        required
        oninput="${({ target: { value } }) => setName(value)}"
        value="${name}"
      />
    </label>
  `;
}

function renderExportButton(maps) {
  return html`
    <button type="button" onclick="${() => exportLandmarks(maps[mapIndex()])}">
      ${i18n("tools.landmarks.add.export")}
    </button>
  `;
}

function exportLandmarks(map) {
  const landmarks = map
    .landmarks()
    .filter(({ type }) => TYPES[type].canBeAddedByUser)
    // remove any property that has been added at runtime
    .map(({ type, name, x_mi, y_mi }) => ({ type, name, x_mi, y_mi }));

  navigator.clipboard
    .writeText(JSON.stringify(landmarks, undefined, "  "))
    .then(() => {});
}

export default function renderAddPane() {
  return renderToolPane(
    isActive,
    i18n("tools.landmarks.add"),
    () => html`
      ${maps.map((maps) => renderMapSelect(maps))}<br />
      ${renderTypeSelect()}<br />
      ${renderNameInput()}<br />
      ${maps.map((maps) => renderExportButton(maps))}
    `,
  );
}
