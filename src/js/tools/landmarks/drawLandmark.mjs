import drawAndRestore from "../../canvas/drawAndRestore.mjs";
import drawHollowCircle from "../../canvas/drawHollowCircle.mjs";
import getCssProperty from "../../css/getCssProperty.mjs";
import convert from "../../css/convert.mjs";
import calculatePositionOnCanvas from "../../canvas/calculatePositionOnCanvas.mjs";
import drawFilledCircle from "../../canvas/drawFilledCircle.mjs";
import TYPES from "./types.mjs";

export const ICON_SIZE =
  convert(getCssProperty("--icon-size"), "rem", "px") / 2;
const BACKGROUND_COLOR = getCssProperty("--background-color");
const BORDER_COLOR = getCssProperty("--color");

export default function drawLandmark(context, view, { type, x_mi, y_mi }) {
  const { image } = TYPES[type];
  const position = calculatePositionOnCanvas(view, { x_mi, y_mi });
  drawFilledCircle(context, position, ICON_SIZE, { color: BACKGROUND_COLOR });
  drawHollowCircle(context, position, ICON_SIZE, { color: BORDER_COLOR });
  drawAndRestore(context, () =>
    context.drawImage(
      image,
      position.x - ICON_SIZE / 2,
      position.y - ICON_SIZE / 2,
      ICON_SIZE,
      ICON_SIZE,
    ),
  );
}
