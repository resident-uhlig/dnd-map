import renderMainToolBar from "./renderMainToolBar.mjs";
import renderToolPanes from "./renderToolPanes.mjs";
import renderToolCanvases from "./renderToolCanvases.mjs";
import html from "../html/html.mjs";
import renderStatusBar from "./renderStatusBar.mjs";

export default function renderTools() {
  return html`
    ${renderToolCanvases()} ${renderMainToolBar()} ${renderToolPanes()}
    ${renderStatusBar()}
  `;
}
