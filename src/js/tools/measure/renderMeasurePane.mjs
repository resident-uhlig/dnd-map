import renderToolPane from "../renderToolPane.mjs";
import { isActive } from "./renderMeasureToolbar.mjs";
import i18n from "../../i18n.mjs";
import html from "../../html/html.mjs";
import measurement, { setMeasurement } from "./measurement.mjs";
import convert from "../../rules/convert.mjs";
import speed from "../../rules/speed.mjs";
import useState from "../../state/useState.mjs";

function calculateDistance(measurement) {
  if (measurement.length === 0) {
    return 0;
  }

  let distance_mi = 0;
  for (let i = 0; i < measurement.length - 1; i++) {
    const start = measurement[i];
    const end = measurement[i + 1];
    distance_mi += Math.sqrt(
      Math.pow(end.x_mi - start.x_mi, 2) + Math.pow(end.y_mi - start.y_mi, 2),
    );
  }

  return distance_mi;
}

function renderTimeForDistance(speed, distance_mi) {
  const time_min = convert(distance_mi, "mile", "minute", speed);
  return html`
    <tr>
      <th>${i18n(speed.name)}</th>
      <td>${i18n("format")(time_min, "minute", "narrow")}</td>
    </tr>
  `;
}

function renderStartStop(isAdding) {
  if (isAdding) {
    return html`
      <button
        type="button"
        onClick="${() => {
          setIsAdding(false);
          // remove floating node
          setMeasurement.pop();
        }}"
      >
        ${i18n("tools.measure_distance.stop")}
      </button>
    `;
  }

  return html`
    <button type="button" onClick="${() => setIsAdding(true)}">
      ${i18n("tools.measure_distance.continue")}
    </button>
  `;
}

const [isAdding, setIsAdding] = useState(true);
const [isAlignToAxis, setIsAlignToAxis] = useState(false);

export { isAdding, isAlignToAxis };

export default function renderMeasurePane() {
  return renderToolPane(
    isActive,
    i18n("tools.measure_distance"),
    () => html`
      ${measurement.map((measurement) => {
        const distance_mi = calculateDistance(measurement);
        return html`
          ${i18n("format")(distance_mi, "mile", "narrow")}
          <table>
            ${renderTimeForDistance(speed.fast, distance_mi)}
            ${renderTimeForDistance(speed.normal, distance_mi)}
            ${renderTimeForDistance(speed.slow, distance_mi)}
          </table>
        `;
      })}

      <label>
        <input
          type="checkbox"
          checked="${isAlignToAxis}"
          onclick="${({ target: { checked } }) => setIsAlignToAxis(checked)}"
        />
        ${i18n("tools.measure_distance.align_to_axis")}
      </label>

      ${isAdding.map(renderStartStop)}

      <button type="button" onClick="${() => setMeasurement.pop()}">
        ${i18n("tools.measure_distance.remove_last")}
      </button>
    `,
  );
}
