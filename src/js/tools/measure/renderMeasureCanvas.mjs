import createFullsizeCanvas from "../../canvas/createFullsizeCanvas.mjs";
import { isActive } from "./renderMeasureToolbar.mjs";
import drawMeasurement from "./drawMeasurement.mjs";
import measurement from "./measurement.mjs";
import { isAdding } from "./renderMeasurePane.mjs";
import useStates from "../../state/useStates.mjs";
import view from "../../maps/view.mjs";

export default function renderMeasureCanvas() {
  const [canvas, requestDraw] = createFullsizeCanvas((context) => {
    if (isActive()) {
      drawMeasurement(context, view(), measurement(), isAdding());
    }
  });

  useStates(view, measurement, isAdding, isAdding)(requestDraw);
  return canvas;
}
