import i18n from "../../i18n.mjs";
import renderToolButton from "../renderToolButton.mjs";
import useState from "../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderMeasureToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/ruler-solid.svg",
    title: i18n("tools.measure_distance"),
    accessKey: "i",
    setIsActive,
  });
}
