import registerKeyBindings from "../registerKeyBindings.mjs";
import { isActive } from "./renderMeasureToolbar.mjs";
import renderKeyBindings from "../renderKeyBindings.mjs";
import i18n from "../../i18n.mjs";
import measurement, { setMeasurement } from "./measurement.mjs";
import calculatePositionOnMap from "../../canvas/calculatePositionOnMap.mjs";
import { isAdding, isAlignToAxis } from "./renderMeasurePane.mjs";
import view from "../../maps/view.mjs";

function calculatePosition(isAlignToAxis, view, points, event) {
  const point = calculatePositionOnMap(view, event);
  return isAlignToAxis ? alignToAxis(points, point) : point;
}

function alignToAxis(points, point) {
  const previous = points[points.length - 1];
  if (!previous) {
    return point;
  }

  const xDiff = Math.abs(point.x_mi - previous.x_mi);
  const yDiff = Math.abs(point.y_mi - previous.y_mi);
  if (xDiff > yDiff) {
    return { x_mi: point.x_mi, y_mi: previous.y_mi };
  }

  return { x_mi: previous.x_mi, y_mi: point.y_mi };
}

export default function renderMeasureKeyBindings() {
  const bindings = [
    {
      event: "click",
      description: i18n("tools.measure_distance.add"),
      callback: (event) => {
        if (isAdding()) {
          const point = calculatePosition(
            isAlignToAxis(),
            view(),
            measurement(),
            event,
          );
          setMeasurement.push(point);
        }
      },
    },
    {
      event: "mousemove",
      callback: (event) => {
        if (isAdding()) {
          const point = calculatePosition(
            isAlignToAxis(),
            view(),
            measurement().slice(0, -1),
            event,
          );
          setMeasurement.splice(-1, 1, point);
        }
      },
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
