import getCssProperty from "../../css/getCssProperty.mjs";
import calculatePositionOnCanvas from "../../canvas/calculatePositionOnCanvas.mjs";
import drawPath from "../../canvas/drawPath.mjs";
import drawLine from "../../canvas/drawLine.mjs";

export default function drawMeasurement(context, view, measurement, isAdding) {
  if (measurement.length === 0) {
    return;
  }

  const style = {
    color: getCssProperty("--active-color"),
    lineWidth: 1,
  };

  const canvas = context.canvas;
  const path = measurement.map((point) =>
    calculatePositionOnCanvas(view, point),
  );
  drawPath(context, path, style);

  if (isAdding) {
    const { width, height } = canvas;
    const { x, y } = path[path.length - 1];
    drawLine(context, { x, y: 0 }, { x, y: height }, style);
    drawLine(context, { x: 0, y }, { x: width, y }, style);
  }
}
