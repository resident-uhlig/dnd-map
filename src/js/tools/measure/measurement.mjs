import useStateArray from "../../state/useStateArray.mjs";

const [measurement, setMeasurement] = useStateArray([]);
export default measurement;
export { setMeasurement };
