import useStateArray from "../../state/useStateArray.mjs";

const [objects, setObjects] = useStateArray([]);
export default objects;
export { setObjects };
