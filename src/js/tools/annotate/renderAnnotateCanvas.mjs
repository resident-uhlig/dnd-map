import createFullsizeCanvas from "../../canvas/createFullsizeCanvas.mjs";
import objects from "./objects.mjs";
import drawObjects from "./drawObjects.mjs";
import useStates from "../../state/useStates.mjs";
import { hoveredObjects } from "./erase/renderEraseKeyBindings.mjs";
import view from "../../maps/view.mjs";

export default function renderAnnotateCanvas() {
  const [canvas, requestDraw] = createFullsizeCanvas((context) => {
    drawObjects(context, view(), objects(), hoveredObjects());
  });

  useStates(view, objects, hoveredObjects)(requestDraw);

  const watched = [];
  objects((objects) => {
    objects
      .filter((object) => !watched.includes(object))
      .forEach((object) => {
        object.state(requestDraw);
        watched.push(object);
      });
  });

  return canvas;
}
