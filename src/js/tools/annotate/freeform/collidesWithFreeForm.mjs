import drawFreeForm from "./drawFreeForm.mjs";
import convert from "../../../css/convert.mjs";
import getCssProperty from "../../../css/getCssProperty.mjs";
import drawAndRestore from "../../../canvas/drawAndRestore.mjs";

const PADDING = convert(getCssProperty("--padding"), "rem", "px");

export default function collidesWithFreeForm(
  context,
  view,
  { clientX, clientY },
  object,
) {
  drawFreeForm(context, view, object, false);

  const lineWidth =
    Math.max(1, (object.width * view.pxPerMi) / object.pxPerMi) + PADDING;
  return drawAndRestore(context, (context) => {
    context.lineWidth = lineWidth;
    return context.isPointInStroke(clientX, clientY);
  });
}
