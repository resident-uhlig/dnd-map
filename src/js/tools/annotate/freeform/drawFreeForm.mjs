import drawPath from "../../../canvas/drawPath.mjs";
import calculatePositionOnCanvas from "../../../canvas/calculatePositionOnCanvas.mjs";
import getCssProperty from "../../../css/getCssProperty.mjs";
import convert from "../../../css/convert.mjs";

const HIGHLIGHT_COLOR = getCssProperty("--highlight-color");
const PADDING = convert(getCssProperty("--padding"), "rem", "px");

export default function drawFreeForm(
  context,
  view,
  { color, width, pxPerMi, state },
  isHovered,
) {
  const nodes = state().map((node) => calculatePositionOnCanvas(view, node));

  const style = {
    color,
    lineWidth: Math.max(1, (width * view.pxPerMi) / pxPerMi),
  };

  if (isHovered) {
    drawPath(context, nodes, {
      ...style,
      color: HIGHLIGHT_COLOR,
      lineWidth: style.lineWidth + PADDING,
    });
  }

  drawPath(context, nodes, style);
}
