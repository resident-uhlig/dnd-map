import renderRadioToolButton from "../../renderRadioToolButton.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderFreeFormToolbar(common) {
  return renderRadioToolButton({
    icon: "css/fontawesome/pen-solid.svg",
    title: i18n("tools.annotate.freeform"),
    name: "annotate-tool",
    value: "free-form",
    common,
    accessKey: "b",
    setIsActive,
  });
}
