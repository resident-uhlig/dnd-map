import renderToolPane from "../../renderToolPane.mjs";
import { isActive } from "./renderFreeFormToolbar.mjs";
import i18n from "../../../i18n.mjs";
import html from "../../../html/html.mjs";
import getCssProperty from "../../../css/getCssProperty.mjs";
import useState from "../../../state/useState.mjs";

const [color, setColor] = useState(getCssProperty("--active-color"));
const [width, setWidth] = useState(1);

export { color, width };

export default function renderFreeFormPane() {
  return renderToolPane(
    isActive,
    i18n("tools.annotate.freeform"),
    () => html`
      <label>
        ${i18n("tools.annotate.freeform.color")}
        <input
          type="color"
          value="${color}"
          onchange="${({ target: { value } }) => setColor(value)}"
        />
      </label>

      <label>
        ${i18n("tools.annotate.freeform.width")}
        <input
          type="range"
          min="1"
          max="100"
          value="${width}"
          oninput="${({ target: { value } }) => setWidth(parseInt(value))}"
        />
      </label>
    `,
  );
}
