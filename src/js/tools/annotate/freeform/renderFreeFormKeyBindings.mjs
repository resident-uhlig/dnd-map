import registerKeyBindings from "../../registerKeyBindings.mjs";
import { isActive } from "./renderFreeFormToolbar.mjs";
import renderKeyBindings from "../../renderKeyBindings.mjs";
import i18n from "../../../i18n.mjs";
import { setObjects } from "../objects.mjs";
import calculatePositionOnMap from "../../../canvas/calculatePositionOnMap.mjs";
import { color, width } from "./renderFreeFormPane.mjs";
import drawFreeForm from "./drawFreeForm.mjs";
import useState from "../../../state/useState.mjs";
import collidesWithFreeForm from "./collidesWithFreeForm.mjs";
import view from "../../../maps/view.mjs";

function createPath({ pxPerMi }, color, width) {
  const [state, setState] = useState([]);
  return {
    pxPerMi,
    color,
    width,
    state,
    setState,
    draw: drawFreeForm,
    collides: collidesWithFreeForm,
  };
}

function addPoint(setNodes, view, event) {
  setNodes((nodes) => [...nodes, calculatePositionOnMap(view, event)]);
}

export default function renderFreeFormKeyBindings() {
  let path;

  function endPath(event) {
    if (path) {
      addPoint(path.setState, view(), event);
      path = undefined;
    }
  }

  const bindings = [
    {
      priority: 1,
      description: i18n("tools.annotate.freeform"),
      event: "mousemove",
      buttons: 1,
      callback: (event) => {
        if (path === undefined) {
          path = createPath(view(), color(), width());
          setObjects.push(path);
        }

        addPoint(path.setState, view(), event);
        return false;
      },
    },
    {
      event: "mouseup",
      callback: endPath,
    },
    {
      event: "touchend",
      touches: 0,
      callback: endPath,
    },
    {
      event: "touchstart",
      touches: 2,
      callback: endPath,
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
