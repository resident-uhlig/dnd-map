export default function drawObjects(context, view, objects, hoveredObjects) {
  objects.forEach((object) =>
    object.draw(context, view, object, hoveredObjects.includes(object)),
  );
}
