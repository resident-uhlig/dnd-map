import renderFreeFormPane from "./freeform/renderFreeFormPane.mjs";

export default function renderAnnotatePane() {
  return renderFreeFormPane();
}
