import renderFreeFormKeyBindings from "./freeform/renderFreeFormKeyBindings.mjs";
import renderEraseKeyBindings from "./erase/renderEraseKeyBindings.mjs";

export default function renderAnnotateKeyBindings() {
  return [renderFreeFormKeyBindings(), renderEraseKeyBindings()];
}
