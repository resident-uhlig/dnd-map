import renderRadioToolButton from "../../renderRadioToolButton.mjs";
import i18n from "../../../i18n.mjs";
import useState from "../../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderEraseToolbar(common) {
  return renderRadioToolButton({
    icon: "css/fontawesome/eraser-solid.svg",
    title: i18n("tools.annotate.delete_object"),
    name: "annotate-tool",
    value: "erase",
    common,
    accessKey: "d",
    setIsActive,
  });
}
