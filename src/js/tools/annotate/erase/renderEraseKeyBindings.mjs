import registerKeyBindings from "../../registerKeyBindings.mjs";
import renderKeyBindings from "../../renderKeyBindings.mjs";
import { isActive } from "./renderEraseToolbar.mjs";
import i18n from "../../../i18n.mjs";
import objects, { setObjects } from "../objects.mjs";
import useState from "../../../state/useState.mjs";
import view from "../../../maps/view.mjs";

const [hoveredObjects, setHoveredObjects] = useState([]);
export { hoveredObjects };

function deleteHoveredObjects(hoveredObjects) {
  setObjects((objects) =>
    objects.filter((object) => !hoveredObjects.includes(object)),
  );
}

function findHoveredObjects(view, objects, event) {
  const canvas = document.createElement("canvas");
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;

  const context = canvas.getContext("2d");
  return objects.filter((object) =>
    object.collides(context, view, event, object),
  );
}

export default function renderEraseKeyBindings() {
  const bindings = [
    {
      event: "click",
      description: i18n("tools.annotate.delete_object"),
      callback: () => deleteHoveredObjects(hoveredObjects()),
    },
    {
      event: "mousemove",
      callback: (event) =>
        setHoveredObjects(findHoveredObjects(view(), objects(), event)),
    },
  ];

  registerKeyBindings(isActive, bindings);
  return renderKeyBindings(isActive, bindings);
}
