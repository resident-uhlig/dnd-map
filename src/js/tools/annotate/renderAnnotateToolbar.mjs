import html from "../../html/html.mjs";
import renderToolBar from "../renderToolBar.mjs";
import renderFreeFormToolbar from "./freeform/renderFreeFormToolbar.mjs";
import renderEraseToolbar from "./erase/renderEraseToolbar.mjs";

export default function renderAnnotateToolbar() {
  const common = {};
  return renderToolBar(
    () => html`
      <li>${renderFreeFormToolbar(common)}</li>
      <li>${renderEraseToolbar(common)}</li>
    `,
  );
}
