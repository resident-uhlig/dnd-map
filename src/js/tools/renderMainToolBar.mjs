import renderToolBar from "./renderToolBar.mjs";
import renderViewToolbar from "././view/renderViewToolbar.mjs";
import renderShareToolbar from "./share/renderShareToolbar.mjs";
import renderMeasureToolbar from "./measure/renderMeasureToolbar.mjs";
import renderLandmarksToolbar from "./landmarks/renderLandmarksToolbar.mjs";
import renderAnnotateToolbar from "./annotate/renderAnnotateToolbar.mjs";
import renderI18nToolbar from "./i18n/renderI18nToolbar.mjs";
import html from "../html/html.mjs";

export default function renderMainToolBar() {
  return renderToolBar(
    () => html`
      <li>${renderViewToolbar()}</li>
      <li>${renderShareToolbar()}</li>
      <li>${renderMeasureToolbar()}</li>
      <li>${renderLandmarksToolbar()}</li>
      <li>${renderAnnotateToolbar()}</li>
      <li>${renderI18nToolbar()}</li>
    `,
    true,
  );
}
