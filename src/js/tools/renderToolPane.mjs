import html from "../html/html.mjs";
import useState from "../state/useState.mjs";
import renderExpandCollapseButton from "../html/renderExpandCollapseButton.mjs";

export default function renderToolPane(isActive, title, renderContent) {
  const [isExpanded, setIsExpanded] = useState(true);
  return isActive.map((isActive) => {
    if (isActive) {
      return html`
        <dt>
          ${renderExpandCollapseButton(isExpanded, setIsExpanded)} ${title}
        </dt>
        <dd
          class="${isExpanded.map((isExpanded) =>
            isExpanded ? "expanded" : "",
          )}"
        >
          ${renderContent()}
        </dd>
      `;
    }
  });
}
