import renderViewKeyBindings from "./view/renderViewKeyBindings.mjs";
import html from "../html/html.mjs";
import renderShareKeyBindings from "./share/renderShareKeyBindings.mjs";
import renderMeasureKeyBindings from "./measure/renderMeasureKeyBindings.mjs";
import renderLandmarksKeyBindings from "./landmarks/renderLandmarksKeyBindings.mjs";
import renderAnnotateKeyBindings from "./annotate/renderAnnotateKeyBindings.mjs";

export default function renderStatusBar() {
  return html`
    <ul class="status-bar">
      ${renderViewKeyBindings()} ${renderShareKeyBindings()}
      ${renderMeasureKeyBindings()} ${renderLandmarksKeyBindings()}
      ${renderAnnotateKeyBindings()}
    </ul>
  `;
}
