import html from "../../html/html.mjs";
import i18n from "../../i18n.mjs";
import { exportMissingKeys } from "../../i18n/de_DE.mjs";
import renderToolPane from "../renderToolPane.mjs";
import { isActive } from "./renderI18nToolbar.mjs";

export default function renderI18nPane() {
  return renderToolPane(
    isActive,
    i18n("tools.language"),
    () => html`
      <button type="button" onclick="${() => exportMissingKeys()}">
        ${i18n("tools.language.export")}
      </button>
    `,
  );
}
