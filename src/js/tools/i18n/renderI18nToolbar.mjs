import i18n from "../../i18n.mjs";
import renderToolButton from "../renderToolButton.mjs";
import useState from "../../state/useState.mjs";

const [isActive, setIsActive] = useState(false);
export { isActive };

export default function renderI18nToolbar() {
  return renderToolButton({
    icon: "css/fontawesome/language-solid.svg",
    title: i18n("tools.language"),
    setIsActive,
  });
}
