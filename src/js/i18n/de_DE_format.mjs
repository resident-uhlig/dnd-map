class CubicFormat {
  #format;

  constructor(unit, unitDisplay) {
    this.#format = getFormatLazy(unit, unitDisplay);
  }

  format(value) {
    return `${this.#format.format(value)}³`;
  }
}

class PintFormat {
  #format;
  #unit;

  constructor(unitDisplay) {
    this.#format = new Intl.NumberFormat("de-DE", { maximumFractionDigits: 1 });
    switch (unitDisplay) {
      case "short":
      case "narrow":
        this.#unit = (_) => " pt";
        break;

      case "long":
        this.#unit = (value) => (value === 1 ? " Pint" : " Pints");
        break;

      default:
        throw new Error(`invalid unitDisplay '${unitDisplay}'`);
    }
  }

  format(value) {
    return `${this.#format.format(value)}${this.#unit(value)}`;
  }
}

const conversions = {
  // distance
  inch: {
    unit: "centimeter",
    map: {
      1: 2.5,
      4: 10,
    },
  },

  foot: {
    unit: "meter",
    map: {
      1: 0.3,
      3: 0.9,
      5: 1.5,
      10: 3,
      15: 4.5,
      20: 6,
      25: 7.5,
      30: 9,
      35: 10.5,
      40: 12,
      50: 15,
      60: 18,
      80: 24,
      90: 27,
      120: 36,
      150: 45,
      300: 90,
      320: 96,
      600: 180,
    },
  },

  mile: {
    unit: "kilometer",
    map: {
      1: 1.5,
      10: 15,
    },
  },

  // volume
  gallon: {
    unit: "liter",
    map: {
      10: 40,
    },
  },

  "cubic-foot": {
    unit: "liter",
    map: {
      0.2: 6,
      1: 30,
    },
  },

  "fluid-ounce": {
    unit: "milliliter",
    map: {
      1: 30,
    },
  },

  pint: {
    unit: "liter",
    map: {
      1: 0.5,
      4: 2,
    },
  },

  // weight
  pound: {
    unit: "kilogram",
    factor: 0.453592,
  },
};

const fractions = {
  // distance
  millimeter: { next: { factor: 10, unit: "centimeter" } },
  centimeter: { next: { factor: 100, unit: "meter" } },
  meter: { next: { factor: 1000, unit: "kilometer" } },

  /*

1 mi = 1.5 km
1 ft = 0.3 m
1 km = 1000 m

x ft = 1 mi = 1.5 km = 1500 m = 5000 ft

*/
  foot: { next: { factor: 5000, unit: "mile" } },

  // volume
  milliliter: { next: { factor: 1000, unit: "liter" } },

  // weight
  gram: { next: { factor: 1000, unit: "kilogram" } },

  // time
  second: { next: { factor: 60, unit: "minute" } },
  minute: { next: { factor: 60, unit: "hour" } },
  hour: { next: { factor: 24, unit: "day" } },
  day: { next: { factor: 7, unit: "week" } },
};

Object.entries(fractions).forEach(
  ([
    unit,
    {
      next: { factor, unit: nextUnit },
    },
  ]) => {
    if (!fractions[nextUnit]) {
      fractions[nextUnit] = {};
    }

    fractions[nextUnit].previous = { factor: factor, unit };
  },
);

function calculateFractions(value, unit) {
  while (fractions[unit] && fractions[unit].previous) {
    value *= fractions[unit].previous.factor;
    unit = fractions[unit].previous.unit;
  }

  const output = [];
  do {
    if (fractions[unit].next) {
      const nextValue = Math.floor(value / fractions[unit].next.factor);
      value = value % fractions[unit].next.factor;
      if (value || (!nextValue && !output.length)) {
        output.unshift({
          unit,
          value,
        });
      }

      if (!nextValue) {
        return output;
      }

      value = nextValue;
      unit = fractions[unit].next.unit;
    } else {
      output.unshift({ unit, value });
      return output;
    }
  } while (true);
}

const nbsp = "\xa0";

const formats = {};

function getFormatLazy(unit, unitDisplay) {
  if (!formats[unit]) {
    formats[unit] = [];
  }

  if (!formats[unit][unitDisplay]) {
    switch (unit) {
      case "cubic-foot":
        formats[unit][unitDisplay] = new CubicFormat("foot", unitDisplay);
        break;

      case "pint":
        formats[unit][unitDisplay] = new PintFormat(unitDisplay);
        break;

      default:
        formats[unit][unitDisplay] = new Intl.NumberFormat("de-DE", {
          style: "unit",
          maximumFractionDigits: 1,
          unit,
          unitDisplay,
        });
    }
  }

  return formats[unit][unitDisplay];
}

export default function format(value, unit, unitDisplay) {
  const formatted = calculateFractions(value, unit)
    .map(({ value, unit }) =>
      getFormatLazy(unit, unitDisplay || "long")
        .format(value)
        .replace(" ", nbsp),
    )
    .join(nbsp);

  if (conversions[unit]) {
    return `${formatted} (${format(
      convert(value, unit),
      conversions[unit].unit,
      unitDisplay,
    )})`;
  }

  return formatted;
}

function convert(value, unit) {
  const factor = conversions[unit].factor;
  if (factor) {
    return value * factor;
  }

  const mapped = conversions[unit].map[value];
  if (mapped) {
    return mapped;
  }

  const [from, to] = Object.entries(conversions[unit].map)[0];
  return (value * to) / from;
}
