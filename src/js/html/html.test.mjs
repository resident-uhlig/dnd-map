import { describe, it } from "../test/test.mjs";
import html from "./html.mjs";
import assert from "../test/assert.mjs";
import useState from ".././state/useState.mjs";

describe("html", () => {
  describe("elements", () => {
    describe("with numeric value", () => {
      it("renders number", () => {
        assert(html`<div>${1}</div>`)
          .innerHtml()
          .isStrictlyEqualTo("<div>1</div>");
      });
    });

    describe("with static string", () => {
      it("renders empty string", () => {
        assert(html``)
          .innerHtml()
          .isStrictlyEqualTo("");
      });

      it("renders text", () => {
        assert(html`text`)
          .innerHtml()
          .isStrictlyEqualTo("text");
      });

      it("escapes special characters", () => {
        assert(html`<&`)
          .innerHtml()
          .isStrictlyEqualTo("&lt;&amp;");
      });

      it("renders one html element", () => {
        assert(html`<div></div>`)
          .innerHtml()
          .isStrictlyEqualTo("<div></div>");
      });

      it("renders two html elements", () => {
        assert(
          html`<div></div>
            <p></p>`,
        ).isSemanticallyEqualToString("<div></div> <p></p>");
      });

      it("renders nested elements", () => {
        assert(html`<div><p></p></div>`)
          .innerHtml()
          .isStrictlyEqualTo("<div><p></p></div>");
      });

      it("renders two html elements, attributes, text nodes, comments", () => {
        assert(
          html`zxc
            <div data-foo="bar">asdf</div>
            qwe<!-- <3 -->rtz
            <p data-baz="qwx">hjkl</p>
            mno`,
        ).isSemanticallyEqualToString(
          "zxc <div data-foo='bar'>asdf</div> qwe<!-- <3 -->rtz <p data-baz='qwx'>hjkl</p> mno",
        );
      });
    });

    describe("with static values", () => {
      it("renders text", () => {
        assert(html`${"text"}`)
          .innerHtml()
          .isStrictlyEqualTo("text");
      });

      it("renders one html element", () => {
        const div = document.createElement("div");
        assert(html`${div}`)
          .innerHtml()
          .isStrictlyEqualTo("<div></div>");
      });

      it("renders two html elements from array", () => {
        const div = document.createElement("div");
        const p = document.createElement("p");
        assert(html`${[div, p]}`)
          .innerHtml()
          .isStrictlyEqualTo("<div></div><p></p>");
      });

      it("renders two html elements from HTMLCollection", () => {
        const parent = document.createElement("div");
        const p = document.createElement("p");
        const span = document.createElement("span");
        parent.append(p, span);
        assert(html`${parent.children}`)
          .innerHtml()
          .isStrictlyEqualTo("<p></p><span></span>");
      });

      it("renders two html elements from NodeList", () => {
        const parent = document.createElement("div");
        const p = document.createElement("p");
        const span = document.createElement("span");
        parent.append(p, span);
        assert(html`${parent.childNodes}`)
          .innerHtml()
          .isStrictlyEqualTo("<p></p><span></span>");
      });
    });

    describe("with state values", () => {
      describe("initial value", () => {
        it("renders text", () => {
          const [get] = useState("text");
          assert(html`${get}`)
            .innerHtml()
            .isStrictlyEqualTo("text");
        });

        it("renders one html element", () => {
          const div = document.createElement("div");
          const [get] = useState(div);
          assert(html`${get}`)
            .innerHtml()
            .isStrictlyEqualTo("<div></div>");
        });

        it("renders two html elements from array", () => {
          const div = document.createElement("div");
          const p = document.createElement("p");
          const [get] = useState([div, p]);
          assert(html`${get}`)
            .innerHtml()
            .isStrictlyEqualTo("<div></div><p></p>");
        });
      });

      describe("update", () => {
        it("renders text", () => {
          // GIVEN
          const initialValue = "initial-text";
          const newValue = "new-text";
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`${get}`;
          set(newValue);

          // THEN
          assert(fragment).innerHtml().isStrictlyEqualTo(newValue);
        });

        it("renders one html element", () => {
          // GIVEN
          const initialValue = document.createElement("div");
          const newValue = document.createElement("p");
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`${get}`;
          set(newValue);

          // THEN
          assert(fragment).innerHtml().isStrictlyEqualTo("<p></p>");
        });

        it("renders two html elements from array", () => {
          // GIVEN
          const initialValue = [
            document.createElement("div"),
            document.createElement("p"),
          ];

          const newValue = [
            document.createElement("span"),
            document.createElement("em"),
          ];

          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`${get}`;
          set(newValue);

          // THEN
          assert(fragment)
            .innerHtml()
            .isStrictlyEqualTo("<span></span><em></em>");
        });

        it("renders text when initialValue was undefined", () => {
          // GIVEN
          const initialValue = undefined;
          const newValue = "new-text";
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`${get}`;
          set(newValue);

          // THEN
          assert(fragment).innerHtml().isStrictlyEqualTo(newValue);
        });

        it("renders text when value was undefined at some later point", () => {
          // GIVEN
          const initialValue = "old-text";
          const newValue = "new-text";
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`${get}`;
          set(undefined);
          set(newValue);

          // THEN
          assert(fragment).innerHtml().isStrictlyEqualTo(newValue);
        });

        it("renders text as child when value was undefined at some later point", () => {
          // GIVEN
          const initialValue = "old-text";
          const newValue = "new-text";
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`<ul>
            ${get}
          </ul>`;
          set(undefined);
          set(newValue);

          // THEN
          assert(fragment).isSemanticallyEqualToString(
            `<ul> ${newValue} </ul>`,
          );
        });

        it("renders array as child when value was empty array at some later point", () => {
          // GIVEN
          const initialValue = [html`<li>old</li>`];
          const newValue = [html`<li>new</li>`];
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`<ul>
            ${get}
          </ul>`;
          set([]);
          set(newValue);

          // THEN
          assert(fragment).isSemanticallyEqualToString(
            `<ul> <li>new</li> </ul>`,
          );
        });

        it("renders html element from fragment", () => {
          // GIVEN
          const initialValue = html`<p></p>`;
          const newValue = html`<div></div>`;
          const [get, set] = useState(initialValue);

          // WHEN
          const fragment = html`${get}`;
          set(newValue);

          // THEN
          assert(fragment).innerHtml().isStrictlyEqualTo("<div></div>");
        });

        it("renders value after update when property has been set", () => {
          // GIVEN
          const [get, set] = useState("old");
          const fragment = html`<input value="${get}" />`;

          // WHEN
          fragment.firstChild.value = "other";
          set("new");

          // THEN
          assert(fragment.firstChild.value).isStrictlyEqualTo("new");
          assert(fragment).innerHtml().isStrictlyEqualTo('<input value="new">');
        });
      });
    });
  });

  describe("attributes", () => {
    describe("with static string", () => {
      it("renders empty attribute", () => {
        assert(html`<input checked />`)
          .innerHtml()
          .isStrictlyEqualTo('<input checked="">');
      });

      it("renders empty string", () => {
        assert(html`<div id=""></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div id=""></div>');
      });

      it("renders text", () => {
        assert(html`<div id="my-id"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div id="my-id"></div>');
      });

      it("escapes special characters", () => {
        assert(html`<div data-html='<&amp;"'></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div data-html="<&amp;&quot;"></div>');
      });
    });

    describe("with static values", () => {
      it("renders empty string", () => {
        assert(html`<div id="${""}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div id=""></div>');
      });

      it("removes undefined", () => {
        assert(html`<div id="${undefined}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo("<div></div>");
      });

      it("renders text", () => {
        assert(html`<div id="${"my-id"}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div id="my-id"></div>');
      });

      it("escapes special characters", () => {
        assert(html`<div data-html="${'<&"'}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div data-html="<&amp;&quot;"></div>');
      });
    });

    describe("with state values", () => {
      describe("initial value", () => {
        it("renders empty string", () => {
          const [get] = useState("");
          assert(html`<div id="${get}"></div>`)
            .innerHtml()
            .isStrictlyEqualTo('<div id=""></div>');
        });

        it("renders undefined", () => {
          const [get] = useState(undefined);
          assert(html`<div id="${get}"></div>`)
            .innerHtml()
            .isStrictlyEqualTo("<div></div>");
        });
      });

      describe("update", () => {
        it("updates string", () => {
          // GIVEN
          const [get, set] = useState("");
          const newValue = "new-value";

          // WHEN
          const fragment = html`<div id="${get}"></div>`;
          set(newValue);

          // THEN
          assert(fragment)
            .innerHtml()
            .isStrictlyEqualTo(`<div id="${newValue}"></div>`);
        });

        it("updates undefined to string", () => {
          // GIVEN
          const [get, set] = useState(undefined);
          const newValue = "new-value";

          // WHEN
          const fragment = html`<div id="${get}"></div>`;
          set(newValue);

          // THEN
          assert(fragment)
            .innerHtml()
            .isStrictlyEqualTo(`<div id="${newValue}"></div>`);
        });
      });
    });

    describe("style", () => {
      it("renders style as static string", () => {
        assert(html`<div style="color: red;"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div style="color: red;"></div>');
      });

      it("renders style as string value", () => {
        assert(html`<div style="${"color: red;"}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div style="color: red;"></div>');
      });

      it("renders style as object value", () => {
        assert(html`<div style="${{ color: "red" }}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div style="color: red;"></div>');
      });

      it("renders style property as state", () => {
        const [get] = useState("red");
        assert(html`<div style="${{ color: get }}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo('<div style="color: red;"></div>');
      });

      it("updates style property as state", () => {
        // GIVEN
        const [get, set] = useState("red");
        const newValue = "green";

        // WHEN
        const fragment = html`<div style="${{ color: get }}"></div>`;
        set(newValue);

        // THEN
        assert(fragment)
          .innerHtml()
          .isStrictlyEqualTo(`<div style="color: ${newValue};"></div>`);
      });

      it("renders style as undefined", () => {
        assert(html`<div style="${undefined}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo("<div></div>");
      });

      it("renders style as empty object", () => {
        assert(html`<div style="${{}}"></div>`)
          .innerHtml()
          .isStrictlyEqualTo("<div></div>");
      });
    });

    describe("checked", () => {
      it("renders checked as static string", () => {
        assert(html`<input checked />`)
          .innerHtml()
          .isStrictlyEqualTo('<input checked="">');
      });

      it("renders checked as true value", () => {
        assert(html`<input checked="${true}" />`)
          .innerHtml()
          .isStrictlyEqualTo('<input checked="">');
      });

      it("renders checked as false value", () => {
        assert(html`<input checked="${false}" />`)
          .innerHtml()
          .isStrictlyEqualTo("<input>");
      });

      it("renders checked as state value true", () => {
        const [get] = useState(true);
        assert(html`<input checked="${get}" />`)
          .innerHtml()
          .isStrictlyEqualTo('<input checked="">');
      });

      it("renders checked as state value true", () => {
        const [get] = useState(false);
        const fragment = html`<input checked="${get}" />`;
        assert(fragment.firstChild.checked).isFalse();
        assert(fragment).innerHtml().isStrictlyEqualTo("<input>");
      });

      it("renders checked as state value true after update", () => {
        // GIVEN
        const [get, set] = useState(false);
        const fragment = html`<input checked="${get}" />`;

        // WHEN
        set(true);

        // THEN
        assert(fragment.firstChild.checked).isTrue();
        assert(fragment).innerHtml().isStrictlyEqualTo('<input checked="">');
      });

      it("renders checked as state value true after update when property has been set", () => {
        // GIVEN
        const [get, set] = useState(false);
        const fragment = html`<input checked="${get}" />`;

        // WHEN
        fragment.firstChild.checked = false;
        set(true);

        // THEN
        assert(fragment.firstChild.checked).isTrue();
        assert(fragment).innerHtml().isStrictlyEqualTo('<input checked="">');
      });

      it("renders checked as state value false after update when property has been set", () => {
        // GIVEN
        const [get, set] = useState(true);
        const fragment = html`<input checked="${get}" />`;

        // WHEN
        fragment.firstChild.checked = true;
        set(false);

        // THEN
        assert(fragment.firstChild.checked).isFalse();
        assert(fragment).innerHtml().isStrictlyEqualTo("<input>");
      });
    });

    describe("event handlers", () => {
      it("binds single event handler", () => {
        // WHEN
        let clicked;
        const fragment = html`<button
          type="button"
          onclick="${() => (clicked = true)}"
        >
          something
        </button>`;
        fragment.firstChild.click();

        // THEN
        assert(fragment).isSemanticallyEqualToString(
          '<button type="button"> something </button>',
        );

        assert(clicked).isTrue();
      });

      it("binds multiple event handlers", () => {
        // WHEN
        let clicked1;
        let clicked2;
        const fragment = html`<button
          type="button"
          onclick="${[() => (clicked1 = true), () => (clicked2 = true)]}"
        >
          something
        </button>`;

        fragment.firstChild.click();

        // THEN
        assert(fragment).isSemanticallyEqualToString(
          '<button type="button"> something </button>',
        );

        assert(clicked1).isTrue();
        assert(clicked2).isTrue();
      });
    });
  });
});
