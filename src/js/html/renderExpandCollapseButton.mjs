import i18n from "../i18n.mjs";
import html from "../html/html.mjs";

export default function renderExpandCollapseButton(isExpanded, setIsExpanded) {
  const classes = isExpanded.map((isExpanded) => {
    if (isExpanded) {
      return "expand-collapse-button expanded";
    }

    return "expand-collapse-button";
  });

  return html`<input
    class="${classes}"
    title="${i18n("expand_collapse")}"
    type="checkbox"
    checked="${isExpanded}"
    onclick="${({ target: { checked } }) => setIsExpanded(checked)}"
  />`;
}
