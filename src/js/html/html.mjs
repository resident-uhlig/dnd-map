const PLACEHOLDER_PREFIX = "placeholder-";

const evaluator = new XPathEvaluator();
const findNodePlaceholders = evaluator.createExpression(
  `//comment()[contains(., '${PLACEHOLDER_PREFIX}')]`,
);
const findAttributePlaceholders = evaluator.createExpression(
  `//@*[contains(., '${PLACEHOLDER_PREFIX}')]`,
);

const matchNodePlaceholder = /(\d+)/;
const matchAttributePlaceholders = new RegExp(
  `<!-- ${PLACEHOLDER_PREFIX}(\\d+) -->`,
);

function replaceNodePlaceholders(template, values) {
  template.content.childNodes.forEach((node) => {
    const snapshot = findNodePlaceholders.evaluate(
      node,
      XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
    );
    for (let i = 0; i < snapshot.snapshotLength; i++) {
      const placeholder = snapshot.snapshotItem(i);
      const index = parseInt(placeholder.data.match(matchNodePlaceholder)[1]);
      const value = values[index];
      insertBefore(value, placeholder);
      placeholder.remove();
    }
  });
}

function insertBefore(value, placeholder) {
  if (typeof value === "function") {
    let current = [placeholder];
    value((value) => {
      const old = current;
      current = insertBefore(value, old[0]);
      old.forEach((old) => old.remove());
    });

    return [];
  }

  if (Array.isArray(value)) {
    value = value.flatMap((value) => insertBefore(value, placeholder));
    if (value.length > 0) {
      return value;
    }

    value = undefined;
  }

  if (value instanceof HTMLCollection || value instanceof NodeList) {
    return insertBefore(Array.from(value), placeholder);
  }

  if (value instanceof DocumentFragment) {
    return insertBefore(Array.from(value.childNodes), placeholder);
  }

  value = mapNodeValue(value);
  console.assert(value instanceof Node);
  placeholder.before(value);
  return [value];
}

function mapNodeValue(value) {
  if (typeof value === "string" || typeof value === "number") {
    return document.createTextNode(value);
  }

  if (value === undefined) {
    return document.createComment("");
  }

  return value;
}

function replaceAttributePlaceholders(template, values) {
  template.content.childNodes.forEach((node) => {
    const snapshot = findAttributePlaceholders.evaluate(
      node,
      XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
    );
    for (let i = 0; i < snapshot.snapshotLength; i++) {
      const placeholder = snapshot.snapshotItem(i);
      const [_, match] = placeholder.value.match(matchAttributePlaceholders);
      setAttribute(placeholder.ownerElement, placeholder.name, values[match]);
    }
  });
}

function setAttribute(element, name, value) {
  if (value === undefined) {
    element.removeAttribute(name);
    return;
  }

  if (typeof value === "function") {
    if (name.substring(0, 2) === "on") {
      element.addEventListener(name.substring(2), value);
      element.removeAttribute(name);
    } else {
      value((value) => {
        setAttribute(element, name, value);
      });
    }

    return;
  }

  if (Array.isArray(value)) {
    value.forEach((value) => setAttribute(element, name, value));
    return;
  }

  if (
    element[name] instanceof CSSStyleDeclaration &&
    typeof value === "object"
  ) {
    element.removeAttribute(name);
    Object.entries(value).forEach(([key, value]) => {
      if (typeof value === "function") {
        value((value) => (element.style[key] = value));
        return;
      }

      element.style[key] = value;
    });

    return;
  }

  if (typeof element[name] === "boolean") {
    if (value) {
      element.setAttribute(name, "");
    } else {
      element.removeAttribute(name);
    }

    element[name] = value;
    return;
  }

  element.setAttribute(name, value);
  if (name in element) {
    element[name] = value;
  }
}

export default function html(strings, ...values) {
  const template = document.createElement("template");
  template.innerHTML = strings.reduce((previous, current, index) => {
    // Array.prototype.reduce starts index at 1 when initialValue is not specified.
    return previous + `<!-- ${PLACEHOLDER_PREFIX}${index - 1} -->` + current;
  });

  replaceNodePlaceholders(template, values);
  replaceAttributePlaceholders(template, values);

  return template.content;
}
