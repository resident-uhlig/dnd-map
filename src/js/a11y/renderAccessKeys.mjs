function showAccessKeys(body) {
  body.classList.add("show-access-keys");
}

function hideAccessKeys(body) {
  body.classList.remove("show-access-keys");
}

export default function renderAccessKeys() {
  const body = document.body;
  document.addEventListener("keydown", (event) => {
    if (event.altKey) {
      event.preventDefault();
      showAccessKeys(body);
    }
  });

  document.addEventListener("keyup", (event) => {
    if (!event.altKey) {
      event.preventDefault();
      hideAccessKeys(body);
    }
  });
}
