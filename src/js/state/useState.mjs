function map(map) {
  const [state, setState] = useState();
  this((value) => setState(map(value)));
  return state;
}

function flatMap(map) {
  const [state, setState] = useState();
  this((value) => map(value)(setState));
  return state;
}

export default function useState(value) {
  const listeners = [];

  function get(listener) {
    if (arguments.length === 0) {
      return value;
    }

    listeners.push(listener);
    listener(value, undefined);
  }

  get.map = map;
  get.flatMap = flatMap;

  return [
    get,
    function (newValue) {
      if (typeof newValue === "function") {
        newValue = newValue(value);
      }

      if (newValue === value) {
        return;
      }

      const oldValue = value;
      value = newValue;
      listeners.forEach((listener) => listener(value, oldValue));
    },
  ];
}
