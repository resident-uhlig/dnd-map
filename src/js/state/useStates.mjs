import useState from "./useState.mjs";

export default function useStates(...states) {
  const [all, setAll] = useState();
  states.forEach((state) =>
    state(() => setAll(states.map((state) => state()))),
  );
  return all;
}
