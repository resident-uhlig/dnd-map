import useState from "./useState.mjs";

export default function useStaticState(value) {
  const [state] = useState(value);
  return state;
}
