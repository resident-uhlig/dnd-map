import useState from "./useState.mjs";

function push(value) {
  this((array) => [...array, value]);
}

function pop() {
  this((array) => array.slice(0, -1));
}

function splice(start, deleteCount, ...items) {
  this((array) => array.toSpliced(start, deleteCount, ...items));
}

export default function useStateArray(value) {
  const [array, setArray] = useState(value);
  setArray.push = push;
  setArray.pop = pop;
  setArray.splice = splice;
  return [array, setArray];
}
