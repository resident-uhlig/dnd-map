import { describe, it } from "../test/test.mjs";
import useState from "./useState.mjs";
import assert from "../test/assert.mjs";

const INITIAL = "initial";
const OTHER = "other";

function map(value) {
  return `MAPPED-${value}`;
}

function mapToState(value) {
  const [state] = useState(map(value));
  return state;
}

describe("useState", () => {
  describe("state", () => {
    describe("get", () => {
      it("gets the initial value", () => {
        const [state] = useState(INITIAL);
        assert(state()).isStrictlyEqualTo(INITIAL);
      });

      it("gets a changed value", () => {
        const [state, setState] = useState(INITIAL);

        setState(OTHER);

        assert(state()).isStrictlyEqualTo(OTHER);
      });
    });

    describe("one listener", () => {
      it("gets the initial value", () => {
        const [state] = useState(INITIAL);

        let actual;
        state((value) => (actual = value));

        assert(actual).isStrictlyEqualTo(INITIAL);
      });

      it("gets a changed value", () => {
        const [state, setState] = useState(INITIAL);

        let actual;
        state((value) => (actual = value));
        setState(OTHER);

        assert(actual).isStrictlyEqualTo(OTHER);
      });
    });

    describe("multiple listeners", () => {
      it("get the initial value", () => {
        const [state] = useState(INITIAL);

        let actual1;
        let actual2;
        state((value) => (actual1 = value));
        state((value) => (actual2 = value));

        assert(actual1).isStrictlyEqualTo(INITIAL);
        assert(actual2).isStrictlyEqualTo(INITIAL);
      });

      it("get the changed value", () => {
        const [state, setState] = useState(INITIAL);

        let actual1;
        let actual2;
        state((value) => (actual1 = value));
        state((value) => (actual2 = value));
        setState(OTHER);

        assert(actual1).isStrictlyEqualTo(OTHER);
        assert(actual2).isStrictlyEqualTo(OTHER);
      });
    });

    describe("map", () => {
      describe("get", () => {
        it("gets the initial value", () => {
          const [state] = useState(INITIAL);
          const mappedState = state.map(map);
          assert(mappedState()).isStrictlyEqualTo(map(INITIAL));
        });

        it("gets a changed value", () => {
          const [state, setState] = useState(INITIAL);
          const mappedState = state.map(map);

          setState(OTHER);

          assert(mappedState()).isStrictlyEqualTo(map(OTHER));
        });
      });

      describe("one listener", () => {
        it("gets the initial value", () => {
          const [state] = useState(INITIAL);
          const mappedState = state.map(map);

          let actual;
          mappedState((value) => (actual = value));

          assert(actual).isStrictlyEqualTo(map(INITIAL));
        });

        it("gets a changed value", () => {
          const [state, setState] = useState(INITIAL);
          const mappedState = state.map(map);

          let actual;
          mappedState((value) => (actual = value));
          setState(OTHER);

          assert(actual).isStrictlyEqualTo(map(OTHER));
        });
      });

      describe("multiple listeners", () => {
        it("get the initial value", () => {
          const [state] = useState(INITIAL);
          const mappedState = state.map(map);

          let actual1;
          let actual2;
          mappedState((value) => (actual1 = value));
          mappedState((value) => (actual2 = value));

          assert(actual1).isStrictlyEqualTo(map(INITIAL));
          assert(actual2).isStrictlyEqualTo(map(INITIAL));
        });

        it("get the changed value", () => {
          const [state, setState] = useState(INITIAL);
          const mappedState = state.map(map);

          let actual1;
          let actual2;
          mappedState((value) => (actual1 = value));
          mappedState((value) => (actual2 = value));
          setState(OTHER);

          assert(actual1).isStrictlyEqualTo(map(OTHER));
          assert(actual2).isStrictlyEqualTo(map(OTHER));
        });
      });
    });

    describe("flatMap", () => {
      describe("get", () => {
        it("gets the initial value", () => {
          const [state] = useState(INITIAL);
          const mappedState = state.flatMap(mapToState);
          assert(mappedState()).isStrictlyEqualTo(map(INITIAL));
        });

        it("gets a changed value", () => {
          const [state, setState] = useState(INITIAL);
          const mappedState = state.flatMap(mapToState);

          setState(OTHER);

          assert(mappedState()).isStrictlyEqualTo(map(OTHER));
        });

        describe("one listener", () => {
          it("gets the initial value", () => {
            const [state] = useState(INITIAL);
            const mappedState = state.flatMap(mapToState);

            let actual;
            mappedState((value) => (actual = value));

            assert(actual).isStrictlyEqualTo(map(INITIAL));
          });

          it("gets a changed value", () => {
            const [state, setState] = useState(INITIAL);
            const mappedState = state.flatMap(mapToState);

            let actual;
            mappedState((value) => (actual = value));
            setState(OTHER);

            assert(actual).isStrictlyEqualTo(map(OTHER));
          });
        });

        describe("multiple listeners", () => {
          it("get the initial value", () => {
            const [state] = useState(INITIAL);
            const mappedState = state.flatMap(mapToState);

            let actual1;
            let actual2;
            mappedState((value) => (actual1 = value));
            mappedState((value) => (actual2 = value));

            assert(actual1).isStrictlyEqualTo(map(INITIAL));
            assert(actual2).isStrictlyEqualTo(map(INITIAL));
          });

          it("get the changed value", () => {
            const [state, setState] = useState(INITIAL);
            const mappedState = state.flatMap(mapToState);

            let actual1;
            let actual2;
            mappedState((value) => (actual1 = value));
            mappedState((value) => (actual2 = value));
            setState(OTHER);

            assert(actual1).isStrictlyEqualTo(map(OTHER));
            assert(actual2).isStrictlyEqualTo(map(OTHER));
          });
        });
      });
    });
  });
});
