import { describe, it } from "../test/test.mjs";
import useState from "./useState.mjs";
import assert from "../test/assert.mjs";
import useStates from "./useStates.mjs";

const INITIAL = "initial";
const OTHER = "other";

describe("useStates", () => {
  describe("get", () => {
    it("gets the initial value", () => {
      const [state] = useState(INITIAL);
      const states = useStates(state);

      const actual = states();

      const expected = [INITIAL];
      assert(actual).hasSameSizeAs(expected);
      assert(actual[0]).isStrictlyEqualTo(expected[0]);
    });

    it("gets a changed value", () => {
      const [state, setState] = useState(INITIAL);
      const states = useStates(state);

      setState(OTHER);
      const actual = states();

      const expected = [OTHER];
      assert(actual).hasSameSizeAs(expected);
      assert(actual[0]).isStrictlyEqualTo(expected[0]);
    });
  });

  describe("one listener", () => {
    it("gets the initial value", () => {
      const [state] = useState(INITIAL);
      const states = useStates(state);

      let actual;
      states((value) => (actual = value));

      const expected = [INITIAL];
      assert(actual).hasSameSizeAs(expected);
      assert(actual[0]).isStrictlyEqualTo(expected[0]);
    });

    it("gets a changed value", () => {
      const [state, setState] = useState(INITIAL);
      const states = useStates(state);

      let actual;
      states((value) => (actual = value));
      setState(OTHER);

      const expected = [OTHER];
      assert(actual).hasSameSizeAs(expected);
      assert(actual[0]).isStrictlyEqualTo(expected[0]);
    });
  });
});
