import useState from ".././state/useState.mjs";
import preloadMaps from ".././maps/preloadMaps.mjs";
import preloadLandmarkIcons from "../tools/landmarks/preloadLandmarkIcons.mjs";

export default function preload() {
  const [isPreloading, setIsPreloading] = useState(true);

  const [queue, setQueue] = useState([]);
  function setProgress(progress) {
    // use a clone to trigger notify of subscribers
    const clone = [...queue()];
    const index = clone.findIndex(({ source }) => source === progress.source);
    if (index === -1) {
      clone.push(progress);
    } else {
      clone[index] = progress;
    }

    setQueue(clone);
  }

  const preloading = Promise.all([
    preloadMaps(setProgress),
    preloadLandmarkIcons(setProgress),
  ]).then(([maps]) => {
    setIsPreloading(false);
    return { maps };
  });

  return [isPreloading, queue, preloading];
}
