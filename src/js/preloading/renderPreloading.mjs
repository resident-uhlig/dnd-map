import html from "../html/html.mjs";

function renderQueue(queue) {
  return queue.map((queue) => {
    if (queue.length === 0) {
      return;
    }

    return queue.filter(({ loaded, total }) => loaded < total).map(renderEntry);
  });
}

function renderEntry({ source, loaded, total }) {
  return html`<li>
    <label>
      ${source}
      <progress value="${loaded}" max="${total}" />
    </label>
  </li>`;
}

export default function renderPreloading(isPreloading, queue) {
  return html`${isPreloading.map((isPreloading) => {
    if (isPreloading) {
      return html`<ul class="preloading">
        ${renderQueue(queue)}
      </ul>`;
    }
  })}`;
}
