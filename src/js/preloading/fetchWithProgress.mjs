export default function fetchWithProgress(source, setProgress) {
  return fetch(source).then(async (response) => {
    const contentLength = +response.headers.get("Content-Length");
    const reader = response.body.getReader();
    const chunks = [];
    let loaded = 0;
    while (true) {
      const { done, value } = await reader.read();
      if (value) {
        loaded += value.length;
      }

      if (done) {
        setProgress({
          source,
          loaded: contentLength,
          total: contentLength,
        });

        break;
      }

      chunks.push(value);
      setProgress({
        source,
        loaded,
        total: contentLength,
      });
    }

    return new Blob(chunks, { type: response.headers.get("Content-Type") });
  });
}
