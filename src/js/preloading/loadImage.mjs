import i18n from "../i18n.mjs";
import fetchWithProgress from "./fetchWithProgress.mjs";

export function loadImage(source, setProgress) {
  return fetchWithProgress(source, setProgress)
    .then((blob) => {
      return new Promise((resolve, reject) => {
        const image = new Image();
        image.onload = () => resolve(image);
        image.onerror = reject;
        image.src = URL.createObjectURL(blob);
      });
    })
    .catch((error) => {
      throw new Error(i18n("loaders.image.error")(source, error));
    });
}
