"use strict";

import renderAccessKeys from "./a11y/renderAccessKeys.mjs";
import renderApp from "./app/renderApp.mjs";
import renderPreloading from "./preloading/renderPreloading.mjs";
import preload from "./preloading/preload.mjs";
import { setMaps } from "./maps/maps.mjs";

document.addEventListener("DOMContentLoaded", () => {
  const [isPreloading, queue, preloading] = preload();
  document.body.append(renderPreloading(isPreloading, queue));
  preloading.then(({ maps }) => {
    setMaps(maps);
    document.body.append(renderApp());
  });

  renderAccessKeys();
});
