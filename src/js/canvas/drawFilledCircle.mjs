import drawAndRestore from "./drawAndRestore.mjs";

export default function drawFilledCircle(context, { x, y }, radius, { color }) {
  drawAndRestore(context, (context) => {
    context.fillStyle = color;
    context.beginPath();
    context.arc(x, y, radius, 0, 2 * Math.PI);
    context.fill();
  });
}
