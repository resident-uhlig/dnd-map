import drawAndRestore from "./drawAndRestore.mjs";

export default function drawPath(context, path, { color, lineWidth }) {
  if (path.length < 2) {
    return;
  }

  drawAndRestore(context, (context) => {
    context.strokeStyle = color;
    context.lineWidth = lineWidth;
    context.beginPath();
    context.moveTo(path[0].x, path[0].y);

    for (let i = 1; i < path.length; i++) {
      context.lineTo(path[i].x, path[i].y);
    }

    context.stroke();
  });
}
