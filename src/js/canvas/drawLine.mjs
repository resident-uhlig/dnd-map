import drawAndRestore from "./drawAndRestore.mjs";

export default function drawLine(
  context,
  { x: x0, y: y0 },
  { x: x1, y: y1 },
  { color, lineWidth },
) {
  drawAndRestore(context, (context) => {
    context.strokeStyle = color;
    context.lineWidth = lineWidth;
    context.beginPath();
    context.moveTo(x0, y0);
    context.lineTo(x1, y1);
    context.stroke();
  });
}
