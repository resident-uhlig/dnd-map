function resizeCanvas(canvas) {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}

export default function createFullsizeCanvas(draw) {
  const canvas = document.createElement("canvas");
  canvas.classList.add("fullsize");

  let animationFrameRequest;
  const requestDraw = () => {
    if (animationFrameRequest) {
      cancelAnimationFrame(animationFrameRequest);
    }

    animationFrameRequest = requestAnimationFrame(() => {
      try {
        const context = canvas.getContext("2d");
        context.clearRect(0, 0, canvas.width, canvas.height);
        draw(context);
      } finally {
        animationFrameRequest = undefined;
      }
    });
  };

  window.addEventListener("resize", () => {
    resizeCanvas(canvas);
    requestDraw();
  });

  resizeCanvas(canvas);
  requestDraw();
  return [canvas, requestDraw];
}
