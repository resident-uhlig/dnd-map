export default function drawAndRestore(context, draw) {
  context.save();
  try {
    return draw(context);
  } finally {
    context.restore();
  }
}
