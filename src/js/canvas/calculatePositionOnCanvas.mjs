export default function calculatePositionOnCanvas(
  { x_mi: cx_mi, y_mi: cy_mi, pxPerMi },
  { x_mi, y_mi },
) {
  return {
    x: window.innerWidth / 2 + (x_mi - cx_mi) * pxPerMi,
    y: window.innerHeight / 2 + (y_mi - cy_mi) * pxPerMi,
  };
}
