import drawAndRestore from "./drawAndRestore.mjs";

export default function drawHollowCircle(
  context,
  { x, y },
  radius,
  { color, lineWidth },
) {
  drawAndRestore(context, (context) => {
    context.strokeStyle = color;
    context.lineWidth = lineWidth;
    context.beginPath();
    context.arc(x, y, radius, 0, 2 * Math.PI);
    context.stroke();
  });
}
