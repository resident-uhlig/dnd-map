export default function calculatePositionOnMap(
  { x_mi: cx_mi, y_mi: cy_mi, pxPerMi },
  { clientX, clientY },
) {
  return {
    x_mi: cx_mi + (clientX - window.innerWidth / 2) / pxPerMi,
    y_mi: cy_mi + (clientY - window.innerHeight / 2) / pxPerMi,
  };
}
