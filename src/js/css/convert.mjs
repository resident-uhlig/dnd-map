const CONVERSIONS = {
  rem: {
    px: {
      factor: parseFloat(getComputedStyle(document.documentElement).fontSize),
    },
  },
};

export default function convert(value, from, to) {
  if (typeof value === "string") {
    value = parseFloat(value.replace(from, "").trim());
  }

  return value * CONVERSIONS[from][to].factor;
}
