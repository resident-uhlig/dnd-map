export default function getCssProperty(name) {
  return getComputedStyle(document.documentElement).getPropertyValue(name);
}
