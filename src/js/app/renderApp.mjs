import html from "../html/html.mjs";
import renderMaps from ".././maps/renderMaps.mjs";
import renderTools from "../tools/renderTools.mjs";
import { maps } from "../maps/maps.mjs";
import {goto} from "../maps/view.mjs";

export default function renderApp() {
  const params = new URLSearchParams(location.search);
  const x_mi = parseFloat(params.get("x_mi"));
  const y_mi = parseFloat(params.get("y_mi"));
  const pxPerMi = parseFloat(params.get("px_per_mi"));
  if (Number.isNaN(x_mi) || Number.isNaN(y_mi) || Number.isNaN(pxPerMi)) {
    const lowRes = maps().filter(({isActive}) => isActive())[0];
    goto({
      x_mi: lowRes.center.x_mi,
      y_mi: lowRes.center.y_mi,
      pxPerMi: lowRes.scale.pxPerMi,
    });
  } else {
    goto({ x_mi, y_mi, pxPerMi });
  }

  return html` ${renderMaps()} ${renderTools()} `;
}
