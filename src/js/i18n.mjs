import de_DE from "./i18n/de_DE.mjs";

export default de_DE;

export function normalizeKey(key) {
  return key.replaceAll(/[^A-Za-z0-9._]+/g, "-");
}
